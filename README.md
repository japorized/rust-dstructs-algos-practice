# Data Structure and Algorithm Practices — with Rust!

[Rustdoc](https://japorized.gitlab.io/blocks/doc/lib/index.html)

---

## Introduction

This project aims to provide itself as a learning/research material, this repo is simply meant to be a practice round for myself (atm!).

Here's the usual disclaimer when it comes to learning/research materials:

> **This is not meant to be used in production**

Whatever that is written here is more so to try things out, implement well-known data structures and algorithms, and not necessarily doing them *The Right Way*.

---

## Getting started

1. [Install Rust](https://www.rust-lang.org/learn/get-started)
2. Clone this repo
3. `cd` to your local copy
4. `cargo test`

### Checking outputs

While there are unit tests that can show the inputs and outputs of the algorithms and behaviors of the data structures, you might want to give arbitrary values (that are still valid input values) and see what you can get.

Use `src/bin/` to write simple programs that will do what you want. See the other files for examples.

To run anything in the `src/bin/` directory:
```
cargo run --bin <filename_without_ext>
```
To pass arguments:
```
cargo run --bin <filename_without_ext> -- <args>
```

### Utilities

You can use [`just`](https://github.com/casey/just) with this project to run various utility commands, many of which are very useful for local development.

**(Optional) Requirements**

- [`entr`](http://eradman.com/entrproject/) — run commands upon file change

Check out the `.justfile` for what's available.

---

# References

The inspiration of this project began from one that was made for [JavaScript](https://github.com/trekhleb/javascript-algorithms).

Here are some of my question banks:
- The project that inspired the creation of this project itself (see above)
- [99 Haskell Problems](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems)
