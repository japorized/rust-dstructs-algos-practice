test_filter := ''

# Show available recipes
default:
  @just --list

open-doc:
  @cargo doc --open

# Watch for file changes and build rustdoc on file change
doc-watch:
  @ls src/**/*.rs | entr -s 'RUSTDOCFLAGS="--html-in-header ./.doc-utils/head.html" cargo doc'

# Test all or optionally by a filter (pass test_filter)
test:
  @cargo test {{test_filter}}

# Watch for file changes and re-run tests on file change (can apply test_filter)
test-watch:
  @ls src/**/*.rs | entr -s 'cargo test {{test_filter}}'

# Format code
format:
  @cargo fmt

# Lint code with clippy
lint:
  @cargo clippy -- -D warnings
