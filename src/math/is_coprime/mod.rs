use crate::math::euclidean_algorithm::gcd_no_recursion;

/// Find out whether 2 given numbers are coprime.
///
/// 2 numbers are said to be coprime if their [gcd] is $1$.
/// Furthermore, if $x$ and $y$ are coprime, we often write $x \bot y$.
/// If $x$ and $y$ are not coprime, we write $x \not\bot y$.
///
/// [gcd]: crate::math::euclidean_algorithm::gcd
pub fn is_coprime(num_a: u64, num_b: u64) -> bool {
    gcd_no_recursion(num_a, num_b) == 1
}

#[cfg(test)]
mod test {
    use super::is_coprime;

    #[test]
    fn check_coprimality() {
        assert!(is_coprime(2, 5));
        assert!(is_coprime(35, 64));
        assert!(!is_coprime(2, 4));
    }
}
