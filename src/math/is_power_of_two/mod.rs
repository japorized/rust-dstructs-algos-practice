pub fn is_power_of_two_increasing_power_ver(num: usize) -> bool {
    let mut index = 1;
    while 2_usize.pow(index) <= num {
        if 2_usize.pow(index) == num {
            return true;
        }
        index += 1;
    }

    false
}

/// Tells you whether a number is a power of two
///
/// The bitwise version to perform this check is quite simple once you're told about it.
/// In particular, notice that powers of two, in binary form, always only have 1 bit that's set.
/// If the number n itself is non-zero, we take the bitwise AND of n with n - 1, and this should
/// give us 0.
/// If the number n itself is zero, we consider 0 to not be a power of 2.
pub fn is_power_of_two_bitwise_ver(num: usize) -> bool {
    if num == 0 {
        return false;
    }

    (num & (num - 1)) == 0
}

#[cfg(test)]
mod test {
    use super::{is_power_of_two_bitwise_ver, is_power_of_two_increasing_power_ver};

    #[test]
    fn test_is_power_of_two_increasing_power_ver() {
        assert_eq!(is_power_of_two_increasing_power_ver(0), false);
        assert_eq!(is_power_of_two_increasing_power_ver(3), false);
        assert_eq!(is_power_of_two_increasing_power_ver(8), true);
        assert_eq!(is_power_of_two_increasing_power_ver(3240), false);
        assert_eq!(is_power_of_two_increasing_power_ver(17179869184), true);
    }

    #[test]
    fn test_is_power_of_two_bitwise_ver() {
        assert_eq!(is_power_of_two_bitwise_ver(0), false);
        assert_eq!(is_power_of_two_bitwise_ver(3), false);
        assert_eq!(is_power_of_two_bitwise_ver(8), true);
        assert_eq!(is_power_of_two_increasing_power_ver(3240), false);
        assert_eq!(is_power_of_two_increasing_power_ver(17179869184), true);
    }
}
