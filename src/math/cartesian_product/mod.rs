use std::collections::HashSet;

#[derive(Debug)]
pub struct CartesianProduct<T, U>
where
    T: Eq + Clone,
    U: Eq + Clone,
{
    elements: Vec<(T, U)>,
}

impl<T, U> CartesianProduct<T, U>
where
    T: Eq + Clone,
    U: Eq + Clone,
{
    pub fn new() -> Self {
        CartesianProduct { elements: vec![] }
    }

    pub fn from(elements: Vec<(T, U)>) -> Self {
        CartesianProduct { elements }
    }

    pub fn add_element(&mut self, new_element: (T, U)) {
        self.elements.push(new_element);
    }

    pub fn has(&self, element: &(T, U)) -> bool {
        self.elements.contains(element)
    }
}

impl<T, U> Default for CartesianProduct<T, U>
where
    T: Eq + Clone,
    U: Eq + Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T, U> PartialEq for CartesianProduct<T, U>
where
    T: Eq + Clone,
    U: Eq + Clone,
{
    fn eq(&self, other: &Self) -> bool {
        if self.elements.len() != other.elements.len() {
            false
        } else {
            for self_elem in &self.elements {
                if !other.has(self_elem) {
                    return false;
                }
            }
            true
        }
    }
}

pub fn cartesian_product<T, U>(set_a: &HashSet<T>, set_b: &HashSet<U>) -> CartesianProduct<T, U>
where
    T: Eq + Clone,
    U: Eq + Clone,
{
    let mut product_elems: CartesianProduct<T, U> = CartesianProduct::new();
    for elem_a in set_a {
        for elem_b in set_b {
            product_elems.add_element((elem_a.clone(), elem_b.clone()));
        }
    }

    product_elems
}

#[cfg(test)]
mod test {
    use super::{cartesian_product, CartesianProduct};
    use std::collections::HashSet;

    #[test]
    fn get_cartesian_product() {
        let a: HashSet<usize> = [1, 2, 3].into();
        let b: HashSet<usize> = [4, 5, 6].into();
        let c_product = cartesian_product(&a, &b);
        assert_eq!(
            c_product,
            CartesianProduct {
                elements: vec![
                    (1, 4),
                    (1, 5),
                    (1, 6),
                    (2, 4),
                    (2, 5),
                    (2, 6),
                    (3, 4),
                    (3, 5),
                    (3, 6)
                ]
            }
        );
        assert_ne!(c_product, CartesianProduct::new());
    }
}
