use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::collections::HashSet;
use std::hash::{self, Hash};

#[derive(Debug)]
pub struct Partition {
    pub values: Vec<usize>,
}

impl Hash for Partition {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        // Kinda sad that the only way to really construct a hash for a Partition is to take the
        // sorted sequence of the values
        let mut values = self.values.clone();
        values.sort_unstable();
        values.hash(state);
    }
}

impl PartialEq for Partition {
    /// Test if the current partition has the same values as the given partition, regardless of the
    /// positions of the values
    fn eq(&self, partition_b: &Self) -> bool {
        // This should be self-explanatory if you think for 5s.
        // If two sets of values don't even have the same length in the first place, of course
        // they're not the same partition!
        if self.values.len() != partition_b.values.len() {
            return false;
        }

        let mut self_values = self.values.clone();
        let partition_b_values = partition_b.values.clone();

        // Iteratively check if the values of b are in a (self).
        // We set the value in a to 0 so that it won't ever be matched again.
        for b_val in partition_b_values {
            if let Some(matching_index) = self_values.iter().position(|a_val| a_val == &b_val) {
                self_values[matching_index] = 0;
            } else {
                return false;
            }
        }

        true
    }
}

impl Eq for Partition {}

// This is an unnecessary trait for this exercise, but it allows for some pretty prints.
impl PartialOrd for Partition {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// This is an unnecessary trait for this exercise, but it allows for some pretty prints.
impl Ord for Partition {
    fn cmp(&self, other: &Self) -> Ordering {
        // Compare the lengths of the partitions first.
        // The longer partition is considered to be lesser than a shorter partition.
        let my_values_length = self.values.len();
        let their_values_length = other.values.len();
        match my_values_length.cmp(&their_values_length) {
            Ordering::Less => return Ordering::Greater,
            Ordering::Equal => {}
            Ordering::Greater => return Ordering::Less,
        }

        let mut my_values = self.values.clone();
        my_values.sort_unstable();
        let mut their_values = other.values.clone();
        their_values.sort_unstable();

        for pair in my_values.iter().zip(&their_values) {
            // Doing the below saves us 2 `.reverse()`s.
            // E.g. compare
            // 1, 1, 3
            // and
            // 1, 2, 2
            // In truth, we're comparing these:
            // 3 + 1 + 1
            // 2 + 2 + 1
            // So the first one should be larger.
            // In essense, we're comparing who will first get the larger number, and that partition
            // will be considered "smaller" (as it has spread out what the later number can get).
            match pair.0.cmp(pair.1) {
                Ordering::Greater => return Ordering::Less,
                Ordering::Less => return Ordering::Greater,
                Ordering::Equal => {}
            };
        }

        Ordering::Equal
    }
}

pub fn get_integer_partition(n: usize) -> HashSet<Partition> {
    let mut partitions: HashSet<Partition> = HashSet::new();

    if n == 1 {
        partitions.insert(Partition { values: vec![1] });
    } else {
        // Let's include the trivial partition here
        partitions.insert(Partition { values: vec![n] });

        // Here's a very important point for this algorithm, out of my own realization.
        // Given a number, aside from the trivial partition (which is just the number itself),
        // it can always be broken down into 2 numbers that sum up to it (duh).
        // If we start from n - 1, the sequence of 2-sums look like this:
        // (n - 1) + 1
        // (n - 2) + 2
        // (n - 3) + 3
        // ...
        // The key point is that once you reach half of the value, the pattern repeats itself, but
        // with the positions of the numbers swapped. E.g. if we have 5, then the sequence is:
        // 4 + 1
        // 3 + 2
        // 2 + 3   <--- Pattern starts repeating here
        // 1 + 4
        // If we have the number 6, then the sequence is:
        // 5 + 1
        // 4 + 2
        // 3 + 3   <--- Pattern starts repeating here
        // 2 + 4
        // 1 + 5
        // Let's now define the halfway point of our value
        let halfway_point = if n % 2 == 0 { n / 2 } else { (n + 1) / 2 };
        // Now we need only iterate from this halfway point to the number itself (less 1, since
        // we've handled the trivial case), to find out all of the 2-sums.

        for first_number in halfway_point..n {
            let second_number = n - first_number;

            // Now to talk about why we need only think about the 2-sum.
            // Well, I, the author, am granted with the knowledge of recursion, so it's somewhat
            // clear why we need only consider a base case, and the recurring pattern.
            // The base case is that the given number is a 1, and we've handled that above.
            //
            // Now for the recurring pattern.
            // Notice that given a 2-sum, if any of the number is greater than 1, we can take its
            // integer partition, and then just sum each of the partition to the other number, to
            // get back our original number n.
            // For example, suppose n = 6. Then 4 + 2 (*) is one our 2-sum partitions.
            // 3 + 1 is one of the 2-sum partitions of 4.
            // The avid reader will now notice that 3 + 1 + 2 is a (3-sum) partition of 6.
            // In fact, here's the integer partition of 4:
            // 4
            // 3 + 1
            // 2 + 2
            // 2 + 1 + 1
            // 1 + 1 + 1 + 1
            // And if you add 2 (from (*)) to each of them, we have
            // 4 + 2
            // 3 + 1 + 2
            // 2 + 2 + 2
            // 2 + 1 + 1 + 2
            // 1 + 1 + 1 + 1 + 2
            // all of which are partitions of 6.
            // We can do the same for the 2 from (*), by taking its integer partition:
            // 4 + 2
            // 4 + 1 + 1
            // Convinced? Now to see how simple this makes our algorithm look like.
            partitions.insert(Partition {
                values: vec![first_number, second_number],
            });

            // Adding these if-statements feels like they should make things faster.
            // The algorith works perfectly without it though.
            if first_number > 1 {
                recursively_construct_partitions(&mut partitions, second_number, first_number);
            }
            if second_number > 1 {
                recursively_construct_partitions(&mut partitions, first_number, second_number);
            }
        }
    }

    partitions
}

fn recursively_construct_partitions(
    partitions: &mut HashSet<Partition>,
    base_number: usize,
    number_to_iterate: usize,
) {
    let sub_partitions = get_integer_partition(number_to_iterate);
    for p in sub_partitions {
        let mut values = vec![base_number];
        values.extend_from_slice(&p.values);
        let extended_partition = Partition { values };

        partitions.insert(extended_partition);
    }
}

#[cfg(test)]
mod test {
    use super::{get_integer_partition, Partition};
    use std::collections::HashSet;

    #[test]
    fn test_partition_is_same() {
        assert_eq!(
            Partition {
                values: vec![1, 2, 3]
            },
            Partition {
                values: vec![1, 3, 2]
            }
        );

        assert_ne!(
            Partition {
                values: vec![1, 2, 3]
            },
            Partition {
                values: vec![4, 3, 2]
            }
        );
    }

    #[test]
    fn test_get_integer_partition() {
        {
            let mut matching_hashset: HashSet<Partition> = HashSet::new();
            matching_hashset.insert(Partition { values: vec![1] });
            assert_eq!(get_integer_partition(1), matching_hashset);
        }
        {
            let mut matching_hashset: HashSet<Partition> = HashSet::new();
            matching_hashset.insert(Partition { values: vec![3] });
            matching_hashset.insert(Partition { values: vec![2, 1] });
            matching_hashset.insert(Partition {
                values: vec![1, 1, 1],
            });
            assert_eq!(get_integer_partition(3), matching_hashset);
        }
        {
            let mut matching_hashset: HashSet<Partition> = HashSet::new();
            matching_hashset.insert(Partition { values: vec![6] });
            matching_hashset.insert(Partition { values: vec![5, 1] });
            matching_hashset.insert(Partition { values: vec![4, 2] });
            matching_hashset.insert(Partition {
                values: vec![4, 1, 1],
            });
            matching_hashset.insert(Partition { values: vec![3, 3] });
            matching_hashset.insert(Partition {
                values: vec![3, 2, 1],
            });
            matching_hashset.insert(Partition {
                values: vec![3, 1, 1, 1],
            });
            matching_hashset.insert(Partition {
                values: vec![2, 2, 2],
            });
            matching_hashset.insert(Partition {
                values: vec![2, 2, 1, 1],
            });
            matching_hashset.insert(Partition {
                values: vec![2, 1, 1, 1, 1],
            });
            matching_hashset.insert(Partition {
                values: vec![1, 1, 1, 1, 1, 1],
            });
            assert_eq!(get_integer_partition(6), matching_hashset);
        }
    }
}
