pub fn osa_distance(str_a: &str, str_b: &str) -> usize {
    let m = str_a.len();
    let n = str_b.len();
    let mut dim: Vec<Vec<usize>> = vec![vec![0; n + 1]; m + 1];

    for (i, row) in dim.iter_mut().enumerate().take(m + 1).skip(1) {
        row[0] = i;
    }

    for j in 1..=n {
        dim[0][j] = j;
    }

    let str_a_bytes = str_a.as_bytes();
    let str_b_bytes = str_b.as_bytes();
    for j in 1..=n {
        for i in 1..=m {
            let substitution_cost = if str_a_bytes[i - 1] == str_b_bytes[j - 1] {
                0
            } else {
                1
            };

            dim[i][j] = *[
                dim[i - 1][j] + 1,
                dim[i][j - 1] + 1,
                dim[i - 1][j - 1] + substitution_cost,
            ]
            .iter()
            .min()
            .unwrap();

            if i > 1
                && j > 1
                && str_a_bytes[i - 1] == str_b_bytes[j - 2]
                && str_a_bytes[i - 2] == str_b_bytes[j - 1]
            {
                dim[i][j] = *[dim[i][j], dim[i - 2][j - 2] + 1].iter().min().unwrap();
            }
        }
    }

    dim[m][n]
}

#[cfg(test)]
mod test {
    use super::osa_distance;

    #[test]
    fn basic_checks() {
        assert_eq!(osa_distance("sitting", "kittens"), 3);
        assert_eq!(osa_distance("Sunday", "Saturday"), 3);
        assert_eq!(osa_distance("Saturday", "Sunday"), 3);
        assert_eq!(osa_distance("John", "Johan"), 1);
        assert_eq!(osa_distance("Robert", "Bobby"), 4);
        assert_eq!(osa_distance("Bard", "Brad"), 1);
    }
}
