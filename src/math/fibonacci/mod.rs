/// Get a number from the Fibonacci sequence by its index using the traditional, intuitive method.
///
/// Rust has probably one of the most intuitive, beautiful, and straightforward syntax for getting
/// any number from the fibonacci sequence, IMO (if you choose to use the not-so-efficient, but
/// almost-a-direct-translation method).
pub fn intuitive(index: usize) -> usize {
    match index {
        0 => 0,
        1 => 1,
        _ => intuitive(index - 1) + intuitive(index - 2),
    }
}

/// Get a number from the Fibonacci sequence by its index using the dynamic programming method.
pub fn dynamic(index: usize) -> usize {
    let index_0 = 0;
    let index_1 = 1;

    match index {
        0 => 0,
        1 => 1,
        _ => {
            let mut a_n_less_1 = index_0;
            let mut a_n = index_1;
            for _ in 2..(index + 1) {
                let tmp = a_n;
                a_n += a_n_less_1;
                a_n_less_1 = tmp;
            }

            a_n
        }
    }
}

/// Get a number from the Fibonacci sequence by its index using the fast doubling method.
///
/// [Source](https://www.nayuki.io/page/fast-fibonacci-algorithms)
pub fn fast_doubling(index: usize) -> usize {
    fast_doubling_core(index).0
}

/// Recursively calculate consecutive Fibonacci pairs using the fast-doubling method.
///
/// This is the core of the fast-doubling method, which is after a heavy optimization through a
/// linear-algebraic approach to calculating Fibonacci numbers.
fn fast_doubling_core(index: usize) -> (usize, usize) {
    match index {
        0 => (0, 1),
        _ => {
            let (a, b) = fast_doubling_core(index / 2);
            let c = a * (b * 2 - a);
            let d = a * a + b * b;
            match index % 2 {
                0 => (c, d),
                _ => (d, c + d),
            }
        }
    }
}

/// Get a number from the Fibonacci sequence by its index using [Binet's Formula](https://en.wikipedia.org/wiki/Fibonacci_number#Closed-form_expression).
pub fn binets(index: usize) -> usize {
    let root_of_five: f64 = 5.0_f64.sqrt();
    let phi = (1.0 + root_of_five) / 2.0;
    let psi = -phi.powi(-1);

    ((phi.powi(index as i32) - psi.powi(index as i32)) / root_of_five).round() as usize
}

/// Get $n$ Fibonacci numbers.
pub fn get_n_numbers(n: usize) -> Vec<usize> {
    let mut second_last_num = 0;
    let mut last_num = 1;

    if n == 0 {
        vec![]
    } else if n == 1 {
        vec![second_last_num]
    } else {
        let mut fibos = vec![second_last_num, last_num];

        for _ in 2..n {
            let tmp = last_num;
            last_num += second_last_num;
            second_last_num = tmp;
            fibos.push(last_num);
        }

        fibos
    }
}

/// Get Fibonacci numbers up to $n$, including $n$ itself.
pub fn get_up_to(max_num: usize) -> Vec<usize> {
    match max_num {
        0 => vec![0],
        1 => vec![0, 1],
        _ => {
            let mut second_last_num = 0;
            let mut last_num = 1;

            let mut fibos = vec![second_last_num, last_num];

            while last_num < max_num {
                let tmp = last_num;
                last_num += second_last_num;
                second_last_num = tmp;
                fibos.push(last_num);
            }

            fibos
        }
    }
}

#[cfg(test)]
mod test {
    use crate::math::fibonacci as fibo;

    #[test]
    fn get_fibos_intuitively() {
        assert_eq!(fibo::intuitive(0), 0);
        assert_eq!(fibo::intuitive(1), 1);
        assert_eq!(fibo::intuitive(2), 1);
        assert_eq!(fibo::intuitive(3), 2);
        assert_eq!(fibo::intuitive(4), 3);
        assert_eq!(fibo::intuitive(10), 55);
    }

    #[test]
    fn get_fibos_dynamically() {
        assert_eq!(fibo::dynamic(0), 0);
        assert_eq!(fibo::dynamic(1), 1);
        assert_eq!(fibo::dynamic(2), 1);
        assert_eq!(fibo::dynamic(3), 2);
        assert_eq!(fibo::dynamic(4), 3);
        assert_eq!(fibo::dynamic(10), 55);
    }

    #[test]
    fn get_fibos_by_fast_doubling() {
        assert_eq!(fibo::fast_doubling(0), 0);
        assert_eq!(fibo::fast_doubling(1), 1);
        assert_eq!(fibo::fast_doubling(2), 1);
        assert_eq!(fibo::fast_doubling(3), 2);
        assert_eq!(fibo::fast_doubling(4), 3);
        assert_eq!(fibo::fast_doubling(10), 55);
    }

    #[test]
    fn get_fibos_by_binets() {
        assert_eq!(fibo::binets(0), 0);
        assert_eq!(fibo::binets(1), 1);
        assert_eq!(fibo::binets(2), 1);
        assert_eq!(fibo::binets(3), 2);
        assert_eq!(fibo::binets(4), 3);
        assert_eq!(fibo::binets(10), 55);
    }

    #[test]
    fn get_fibos_up_till_index() {
        assert_eq!(fibo::get_n_numbers(0), []);
        assert_eq!(fibo::get_n_numbers(1), [0]);
        assert_eq!(fibo::get_n_numbers(2), [0, 1]);
        assert_eq!(fibo::get_n_numbers(3), [0, 1, 1]);
        assert_eq!(fibo::get_n_numbers(4), [0, 1, 1, 2]);
        assert_eq!(
            fibo::get_n_numbers(13),
            [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
        );
    }

    #[test]
    fn get_fibos_up_to_n() {
        assert_eq!(fibo::get_up_to(0), [0]);
        assert_eq!(fibo::get_up_to(1), [0, 1]);
        assert_eq!(fibo::get_up_to(2), [0, 1, 1, 2]);
        assert_eq!(
            fibo::get_up_to(144),
            [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
        );
    }
}
