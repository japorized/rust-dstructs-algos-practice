//! # Goldbach's Conjecture
//!
//! Goldbach conjectures that every even positive number greater than 2 is a sum of 2 primes.
//! It has been numerically confirmed to be true up to very large numbers.

use crate::math::sieve_of_eratosthenes::get_prime_numbers_up_to_n;
use std::fmt;

/// Possible error cases when finding the 2 primes that sums up to be a given number.
#[derive(Debug, PartialEq, Eq)]
pub enum GoldbachError {
    NonEvenArgumentNumber,
    LessThanEqualToTwo,
    NoPrimeSum,
}

impl fmt::Display for GoldbachError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            GoldbachError::NonEvenArgumentNumber => {
                write!(f, "Given argument is not an even number")
            }
            GoldbachError::LessThanEqualToTwo => {
                write!(f, "Given number is lesser than or equal to 2")
            }
            GoldbachError::NoPrimeSum => {
                write!(f, "No prime sum")
            }
        }
    }
}

/// Get the two-sum comprised of prime numbers
pub fn goldbach(num: u64) -> Result<(u64, u64), GoldbachError> {
    if num <= 2 {
        Err(GoldbachError::LessThanEqualToTwo)
    } else if num % 2 != 0 {
        Err(GoldbachError::NonEvenArgumentNumber)
    } else {
        let primes = get_prime_numbers_up_to_n(num);
        for i in 0..primes.len() {
            let diff = num - primes[i];
            let prime_index = primes.binary_search(&diff);
            if let Ok(j) = prime_index {
                return Ok((primes[i], primes[j]));
            }
        }

        Err(GoldbachError::NoPrimeSum)
    }
}

/// Respond with whether the given number is a sum of 2 primes based on Goldbach's Conjecture.
pub fn has_prime_sum(num: u64) -> bool {
    goldbach(num).is_ok()
}

#[cfg(test)]
mod test {
    use super::{goldbach, has_prime_sum, GoldbachError};

    #[test]
    fn check_goldbach() {
        assert_eq!(goldbach(28).ok(), Some((5, 23)));
        assert_eq!(goldbach(10).ok(), Some((3, 7)));
        assert_eq!(goldbach(0).err(), Some(GoldbachError::LessThanEqualToTwo));
        assert_eq!(goldbach(2).err(), Some(GoldbachError::LessThanEqualToTwo));
        assert_eq!(
            goldbach(3).err(),
            Some(GoldbachError::NonEvenArgumentNumber)
        );
    }

    #[test]
    fn check_has_prime_sum() {
        assert!(has_prime_sum(4));
        assert!(has_prime_sum(10592));
    }
}
