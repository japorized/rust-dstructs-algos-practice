pub fn classic_loop(n: u128) -> u128 {
    let mut ans: u128 = 1;
    for i in 2..=n {
        ans *= i;
    }

    ans
}

pub fn classic_recursion(n: u128) -> u128 {
    match n {
        0 => 1,
        1 => 1,
        _ => n * classic_recursion(n - 1),
    }
}

#[cfg(test)]
mod test {
    use super::{classic_loop, classic_recursion};

    #[test]
    fn check_classic_loop() {
        let input_and_answers = vec![
            (0, 1),
            (1, 1),
            (2, 2),
            (3, 6),
            (4, 24),
            (5, 120),
            (6, 720),
            (7, 5040),
            (8, 40320),
            (9, 362880),
            (10, 3628800),
            (11, 39916800),
            (12, 479001600),
            (13, 6227020800),
            (14, 87178291200),
            (15, 1307674368000),
        ];

        for ia in input_and_answers.iter() {
            assert_eq!(classic_loop(ia.0), ia.1);
        }
    }

    #[test]
    fn check_classic_recursion() {
        let input_and_answers = vec![
            (0, 1),
            (1, 1),
            (2, 2),
            (3, 6),
            (4, 24),
            (5, 120),
            (6, 720),
            (7, 5040),
            (8, 40320),
            (9, 362880),
            (10, 3628800),
            (11, 39916800),
            (12, 479001600),
            (13, 6227020800),
            (14, 87178291200),
            (15, 1307674368000),
        ];

        for ia in input_and_answers.iter() {
            assert_eq!(classic_recursion(ia.0), ia.1);
        }
    }
}
