/// Get the greatest common divisor (GCD) between two positive integers.
///
/// # Definitions
/// ## Greatest Common Divisor (GCD)
/// Given two positive integers $a$ and $b$, the greatest common divisor is the largest positive
/// integer that divides both $a$ and $b$.
///
/// ## Quotients and Remainders
/// Given 2 integers, we can always find 2 integers, $q$ and $r$, that such that we can express
/// their relationship through the following linear equation.
/// $$
/// a = qb + r
/// $$
/// We call $q$ the **quotient**, and $r$ the **remainder**.
///
/// You may notice that the linear equation comes from [Euclid's Division Lemma](https://en.wikipedia.org/wiki/Euclidean_division).
/// For succinctness in the rest of the document, let's call this linear equation as Equation A.
///
/// # The Euclidean Algorithm
/// The Euclidean algorithm is a well-known algorithm that tell us what the GCD of any two positive
/// numbers are.
///
/// The algorithm proceeds as follows.
/// 1. Construct Equation A for $a$ and $b$ to get $q_0$ and $r_0$.
///    $$
///         a = q_0 b + r_0
///    $$
/// 2. Construct Equation A for $b$ and $r_0$ to get $q_1$ and $r_1$.
///    $$
///         b = q_1 r_0 + r_1
///    $$
/// 3. Construct Equation A for $r_0$ and $r_1$ to get $q_2$ and $r_2$.
///    $$
///         r_0 = q_2 r_1 + r_2
///    $$
/// 4. Continue constructing $q_n$ and $r_n$ for as many $n \in \mathbb{N}$ as necessary, until
///    $r_{n_0} = 0$ for some $n_0 \in \mathbb{N}$.
/// 5. If $r_{n_0} = 0$ for some $n_0 \in \mathbb{N}$, then the GCD of $a$ and $b$ is $r_{n_0 -
///    1}$.
///
/// **Notes**:
/// - If $a < b$, then the first step will swap the numbers.
///
/// **References**
/// - [Wikipedia — Euclidean algorithm](https://en.wikipedia.org/wiki/Euclidean_algorithm)
pub fn gcd(a: u64, b: u64) -> u64 {
    if a == 0 {
        b
    } else if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

pub fn gcd_no_recursion(a: u64, b: u64) -> u64 {
    if a == 0 {
        return b;
    }

    let mut last_remainder = a;
    let mut remainder = b;

    while remainder != 0 {
        let tmp = remainder;
        remainder = last_remainder % remainder;
        last_remainder = tmp;
    }

    last_remainder
}

#[cfg(test)]
mod test {
    use super::{gcd, gcd_no_recursion};

    #[test]
    fn find_greatest_common_divisor() {
        assert_eq!(gcd(4, 6), 2);
        assert_eq!(gcd(3, 1), 1);
        assert_eq!(gcd(45, 15), 15);
        assert_eq!(gcd(25, 18), 1);
        assert_eq!(gcd(1071, 462), 21);

        assert_eq!(gcd(0, 6), 6);
    }

    #[test]
    fn find_greatest_common_divisor_without_recursion() {
        assert_eq!(gcd_no_recursion(4, 6), 2);
        assert_eq!(gcd_no_recursion(3, 1), 1);
        assert_eq!(gcd_no_recursion(45, 15), 15);
        assert_eq!(gcd_no_recursion(25, 18), 1);
        assert_eq!(gcd_no_recursion(1071, 462), 21);

        assert_eq!(gcd_no_recursion(0, 6), 6);
    }
}
