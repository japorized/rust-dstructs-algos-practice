use crate::math::sieve_of_eratosthenes::get_prime_numbers_up_to_n;

/// Get the prime factors of a given number.
///
/// Every positive number can be expressed as a unique prime factorization as such:
/// $$
///   2^{a_0} 3^{a_1} 5^{a_2} 7^{a_3} 11^{a_4} \cdots
/// $$
/// where
/// $$
///   \forall i \in \mathbb{N} \quad a_i \in \mathbb{N}
/// $$
///
/// For instance,
/// $$
///   2 = 2^1 \cdot 3^0 \cdot 5^0 \cdots = 2^1
/// $$
/// and
/// $$
///   315 = 3^2 \cdot 5 \cdot 7 = 3 \cdot 3 \cdot 5 \cdot 7
/// $$
///
/// ## Example usage and output
/// ```
/// use lib::math::prime_factors::get_prime_factors;
///
/// assert_eq!(get_prime_factors(315), [3, 3, 5, 7]);
/// ```
pub fn get_prime_factors(n: u64) -> Vec<u64> {
    let possible_primes = get_prime_numbers_up_to_n(n + 1); // Adding 1 cause we want to include the very number itself
    let mut remainder = n;
    let mut prime_factors: Vec<u64> = vec![];

    for prime in possible_primes {
        while remainder % prime == 0 {
            prime_factors.push(prime);
            remainder /= prime;
        }
        if remainder == 0 {
            break;
        }
    }

    prime_factors
}

/// Get prime factors and their multiplicities for a given number
///
/// The prime factors and their multiplicities are returned as pairs, where the first item is the
/// prime factor, and the second item is its multiplicity.
///
/// The multiplicity of a prime factor is simply the number of times the prime factor divides the
/// given number.
///
/// See also [`get_prime_factors`].
///
/// ## Example usage and output
/// ```
/// use lib::math::prime_factors::get_prime_factor_multiplicities;
///
/// assert_eq!(get_prime_factor_multiplicities(315), [(3, 2), (5, 1), (7, 1)]);
/// ```
pub fn get_prime_factor_multiplicities(n: u64) -> Vec<(u64, u64)> {
    let possible_primes = get_prime_numbers_up_to_n(n + 1); // Adding 1 cause we want to include the very number itself
    let mut remainder = n;
    let mut prime_factor_multiplicities: Vec<(u64, u64)> = vec![];

    for prime in possible_primes {
        let mut prime_factor_multiplicity = (prime, 0);
        while remainder % prime == 0 {
            prime_factor_multiplicity.1 += 1;
            remainder /= prime;
        }
        if prime_factor_multiplicity.1 > 0 {
            prime_factor_multiplicities.push(prime_factor_multiplicity);
        }
        if remainder == 0 {
            break;
        }
    }

    prime_factor_multiplicities
}

#[cfg(test)]
mod test {
    use super::{get_prime_factor_multiplicities, get_prime_factors};

    #[test]
    fn check_prime_factors() {
        assert_eq!(get_prime_factors(1), []);
        assert_eq!(get_prime_factors(5), [5]);
        assert_eq!(get_prime_factors(36), [2, 2, 3, 3]);
    }

    #[test]
    fn check_prime_factor_multiplicities() {
        assert_eq!(get_prime_factor_multiplicities(1), []);
        assert_eq!(get_prime_factor_multiplicities(5), [(5, 1)]);
        assert_eq!(get_prime_factor_multiplicities(36), [(2, 2), (3, 2)]);
    }
}
