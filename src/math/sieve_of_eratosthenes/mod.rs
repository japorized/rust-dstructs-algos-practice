use std::convert::TryInto;

/// Return prime numbers up till the given number
/// This algorithm uses the Sieve of Eratosthenes
pub fn get_prime_numbers_up_to_n(n: u64) -> Vec<u64> {
    if n < 2 {
        return vec![];
    }

    let usized_n: usize = n.try_into().unwrap();
    let mut primes: Vec<u64> = vec![];
    let mut bool_arr: Vec<bool> = vec![true; usized_n];
    bool_arr[0] = false;
    bool_arr[1] = false;

    for i in 2..usized_n {
        if bool_arr[i] {
            primes.push(i.try_into().unwrap());
            let mut c = i;
            while c * i <= (n - 1).try_into().unwrap() {
                bool_arr[c * i] = false;
                c += 1;
            }
        }
    }

    primes
}

#[cfg(test)]
mod test {
    use super::get_prime_numbers_up_to_n;

    #[test]
    fn try_sieve_of_eratosthenes() {
        let input_and_answers = vec![
            (20, vec![2, 3, 5, 7, 11, 13, 17, 19]),
            (30, vec![2, 3, 5, 7, 11, 13, 17, 19, 23, 29]),
            (
                120,
                vec![
                    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73,
                    79, 83, 89, 97, 101, 103, 107, 109, 113,
                ],
            ),
        ];

        for ia in input_and_answers.iter() {
            assert_eq!(get_prime_numbers_up_to_n(ia.0), ia.1);
        }
    }
}
