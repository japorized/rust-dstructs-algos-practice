use crate::math::sieve_of_eratosthenes::get_prime_numbers_up_to_n;

/// Get the prime numbers in a given range.
///
/// In other words, this function generates a range of prime numbers within the given bounds.
pub fn get_prime_range(lower_bound: u64, upper_bound: u64) -> Vec<u64> {
    get_prime_numbers_up_to_n(upper_bound + 1)
        .into_iter()
        .filter(|p| p >= &lower_bound)
        .collect()
}

#[cfg(test)]
mod test {
    use super::get_prime_range;

    #[test]
    fn get_prime_ranges() {
        let input_and_answers = vec![
            ((7, 20), vec![7, 11, 13, 17, 19]),
            ((12, 30), vec![13, 17, 19, 23, 29]),
            (
                (18, 120),
                vec![
                    19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
                    103, 107, 109, 113,
                ],
            ),
        ];

        for ia in input_and_answers.iter() {
            assert_eq!(get_prime_range(ia.0 .0, ia.0 .1), ia.1);
        }
    }
}
