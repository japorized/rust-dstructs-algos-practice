/// The Levenshtein function, implemented using the Wagner-Fischer Algorithm.
/// See pseudocode on [Wikipedia](https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm).
pub fn lev(str_a: &str, str_b: &str) -> usize {
    let m = str_a.len();
    let n = str_b.len();
    let mut dim: Vec<Vec<usize>> = vec![vec![0; n + 1]; m + 1];

    for (i, row) in dim.iter_mut().enumerate().take(m + 1).skip(1) {
        row[0] = i;
    }

    for j in 1..=n {
        dim[0][j] = j;
    }

    let str_a_bytes = str_a.as_bytes();
    let str_b_bytes = str_b.as_bytes();
    for j in 1..=n {
        for i in 1..=m {
            let substitution_cost = if str_a_bytes[i - 1] == str_b_bytes[j - 1] {
                0
            } else {
                1
            };

            dim[i][j] = *[
                dim[i - 1][j] + 1,
                dim[i][j - 1] + 1,
                dim[i - 1][j - 1] + substitution_cost,
            ]
            .iter()
            .min()
            .unwrap();
        }
    }

    dim[m][n]
}

/// The Levenshtein function, implemented as-is (a piecewise function).
pub fn lev_orig(str_a: &str, str_b: &str) -> usize {
    if str_b.is_empty() {
        str_a.len()
    } else if str_a.is_empty() {
        str_b.len()
    } else if str_a.as_bytes()[0] == str_b.as_bytes()[0] {
        lev_orig(str_a.get(1..).unwrap(), str_b.get(1..).unwrap())
    } else {
        1 + [
            lev_orig(str_a.get(1..).unwrap(), str_b),
            lev_orig(str_a, str_b.get(1..).unwrap()),
            lev_orig(str_a.get(1..).unwrap(), str_b.get(1..).unwrap()),
        ]
        .iter()
        .min()
        .unwrap()
    }
}

#[cfg(test)]
mod test {
    use super::{lev, lev_orig};

    #[test]
    fn basic_checks() {
        assert_eq!(lev("sitting", "kittens"), 3);
        assert_eq!(lev("Sunday", "Saturday"), 3);
        assert_eq!(lev("Saturday", "Sunday"), 3);
        assert_eq!(lev("John", "Johan"), 1);
        assert_eq!(lev("Robert", "Bobby"), 4);
        assert_eq!(lev("Bard", "Brad"), 2);
    }

    #[test]
    fn test_naive() {
        assert_eq!(lev_orig("sitting", "kittens"), 3);
        assert_eq!(lev_orig("Sunday", "Saturday"), 3);
        assert_eq!(lev_orig("Saturday", "Sunday"), 3);
        assert_eq!(lev_orig("John", "Johan"), 1);
        assert_eq!(lev_orig("Robert", "Bobby"), 4);
        assert_eq!(lev_orig("Bard", "Brad"), 2);
    }
}
