use std::fmt;

#[derive(Debug)]
pub enum EuclideanDistanceErrorType {
    DimensionMismatch,
}

impl fmt::Display for EuclideanDistanceErrorType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            EuclideanDistanceErrorType::DimensionMismatch => write!(f, "Dimensions Mismatch"),
        }
    }
}

#[derive(Debug)]
pub struct EuclideanDistanceError {
    pub a: Vec<f64>,
    pub b: Vec<f64>,
    pub error_type: EuclideanDistanceErrorType,
}

impl fmt::Display for EuclideanDistanceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "a: ({}), b: ({})",
            self.a
                .iter()
                .map(|x| format!("{}", x))
                .collect::<Vec<String>>()
                .join(", "),
            self.b
                .iter()
                .map(|x| format!("{}", x))
                .collect::<Vec<String>>()
                .join(", "),
        )
    }
}

pub fn euclidean_distance(a: Vec<f64>, b: Vec<f64>) -> Result<f64, EuclideanDistanceError> {
    if a.len() != b.len() {
        return Err(EuclideanDistanceError {
            a,
            b,
            error_type: EuclideanDistanceErrorType::DimensionMismatch,
        });
    }

    let mut sum_of_squared_diffs = 0.0;
    for i in 0..a.len() {
        let coord_for_a = match a.get(i) {
            Some(v) => *v,
            None => {
                return Err(EuclideanDistanceError {
                    a,
                    b,
                    error_type: EuclideanDistanceErrorType::DimensionMismatch,
                });
            }
        };
        let coord_for_b = match b.get(i) {
            Some(v) => *v,
            None => {
                return Err(EuclideanDistanceError {
                    a,
                    b,
                    error_type: EuclideanDistanceErrorType::DimensionMismatch,
                });
            }
        };
        sum_of_squared_diffs += (coord_for_a - coord_for_b).powf(2.0);
    }

    Ok(f64::sqrt(sum_of_squared_diffs))
}

#[cfg(test)]
mod test {
    use super::{euclidean_distance, EuclideanDistanceError};

    #[test]
    fn test_euclidean_distance() -> Result<(), EuclideanDistanceError> {
        assert_eq!(euclidean_distance(vec![4.0, 0.0], vec![7.0, 4.0])?, 5.0);
        assert_eq!(
            (euclidean_distance(vec![5.7, 3.44], vec![4.1, 10.52])? * 100.0).round() / 100.0,
            7.26
        );
        assert_eq!(
            (euclidean_distance(vec![0.0, 0.0, 0.0], vec![3.0, 4.0, 5.0])? * 100.0).round() / 100.0,
            7.07
        );

        Ok(())
    }

    #[test]
    #[should_panic]
    fn test_bad_euclidean_distance_inputs() {
        match euclidean_distance(vec![0.0, 0.0], vec![2.0, 3.0, 4.0]) {
            Ok(_) => assert!(false),
            Err(e) => panic!("Reason: {}", e),
        }
    }
}
