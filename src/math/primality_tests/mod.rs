use crate::math::sieve_of_eratosthenes::get_prime_numbers_up_to_n;

/// Get the primality the given number using the [trial division](https://en.wikipedia.org/wiki/Trial_division) method.
pub fn is_prime(n: u64) -> bool {
    // We'll handle anything that's divisible by 2 first, cause those are quick and easy
    if n == 2 {
        return true;
    } else if n % 2 == 0 {
        return false;
    }

    let sqrt_n = f64::sqrt(n as f64);
    let primes_to_sqrt_n = get_prime_numbers_up_to_n(sqrt_n.floor() as u64);
    for prime in primes_to_sqrt_n {
        if n % prime == 0 {
            return false;
        }
    }

    true
}

#[cfg(test)]
mod test {
    use super::is_prime;

    #[test]
    fn trial_division() {
        assert!(is_prime(3));
        assert!(is_prime(2));
        assert!(is_prime(19));
        assert!(is_prime(101));

        assert!(!is_prime(4));
        assert!(!is_prime(8));
        assert!(!is_prime(45));
    }
}
