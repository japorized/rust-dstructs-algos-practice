/// Get the next sparse number given a number $n$. If $n$ itself is sparse, then $n$ itself is
/// returned.
///
/// # Definition
///
/// ## Sparse Number
/// A number is said to be sparse if its binary representation does not have any adjacent 1's.
///
/// For example,
/// - 4 — 100 is sparse
/// - 5 — 101 is sparse
/// - 6 — 110 is not sparse
/// - 7 — 111 is not sparse
/// - 8 — 1000 is sparse
///
/// # Implementation
/// The implementation takes advantage of bit-shifting operations.
///
/// The following is a human-friendly pseudo-code of the algorithm:
/// ```plaintext
/// generate an array of bits based on each bit of the given number
/// add a trailing 0 bit to the end of the array
///
/// for each bit in the array
///   if two consecutive 1's are found, get the index i of the next zero
///     flip all bits before i, and flip i to 1
///     start next iteration from i
///   end if
/// end for
///
/// reconstruct the next sparse number from the bits in the array
/// ```
///
/// Here's an example of how the algorithm runs. Suppose we're given the number 44.
/// 1. $44$ is `101100`. The generated array will be `[0, 0, 1, 1, 0, 1, 0]`.
/// 2. We'll first encounter a duplicate `1` at index 3 (not 2). The next zero from index 3 is at
///    index 4.
/// 3. Flip all bits before index 4 to `0`. The array is now `[0, 0, 0, 0, 1, 1, 0]`. Continue to
///    next iteration.
/// 4. Next encounter of a duplicate `1` is at index 5. Next `0` from 5 is at 6.
/// 5. Flip all bits before 6 to `0`. The array is now `[0, 0, 0, 0, 0, 0, 1]`. Continue to next
///    iteration.
/// 6. Iteration is at the end of the array.
/// 7. Reconstruct the number from `1000000`.
pub fn get_next_sparse_number(n: usize) -> usize {
    let mut bit_rep: Vec<u8> = vec![];
    let mut n1 = n;

    while n1 != 0 {
        bit_rep.push(if n1 & 1 == 0 { 0 } else { 1 });
        n1 >>= 1;
    }

    // Add this at the tail so that we can handle adjacent 1's at the end
    bit_rep.push(0);

    let bit_rep_len = bit_rep.len();
    let mut last_bit_was_one = false;
    for i in 0..bit_rep_len {
        if bit_rep[i] == 1 {
            if last_bit_was_one {
                if let Some(next_zero_in_subarr) =
                    bit_rep.get(i..).unwrap().iter().position(|b| b == &0)
                {
                    let next_zero = next_zero_in_subarr + i;
                    bit_rep[next_zero] = 1;
                    for bit_rep_item in bit_rep.iter_mut().take(next_zero) {
                        *bit_rep_item = 0;
                    }
                    last_bit_was_one = false;
                };
            } else {
                last_bit_was_one = true;
            }
        } else {
            last_bit_was_one = false;
        }
    }

    let mut num = 0;
    for (i, item) in bit_rep.iter().enumerate().take(bit_rep_len) {
        num += item * (1 << i);
    }

    num.into()
}

#[cfg(test)]
mod test {
    use super::get_next_sparse_number;

    #[test]
    fn check_sparse_number_getter() {
        assert_eq!(get_next_sparse_number(6), 8);
        assert_eq!(get_next_sparse_number(4), 4);
        assert_eq!(get_next_sparse_number(38), 40);
        assert_eq!(get_next_sparse_number(44), 64);
    }
}
