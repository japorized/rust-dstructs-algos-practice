use crate::math::{is_coprime::is_coprime, prime_factors::get_prime_factor_multiplicities};

/// Calculate Euler's totient function $\phi(n)$.
///
/// Euler's totient function $\phi$ for a given $n \in \mathbb{N} \setminus \\{ 0 \\}$ is
/// $$
///   \phi(n) := \\{ x \in \mathbb{N} \setminus \\{ 0 \\} \\,|\\, x \bot n \\}
/// $$
/// where $x \bot n$ means that $x$ and $n$ are [coprimes](is_coprime).
pub fn totient_phi(n: u64) -> u64 {
    let mut coprimes_count = 0;

    if is_coprime(1, n) {
        coprimes_count += 1;
    }

    for i in 2..n {
        if is_coprime(i, n) {
            coprimes_count += 1;
        }
    }

    coprimes_count
}

/// Calculate Euler's totient function $\phi(n)$.
/// See main explanation about Euler's totient function in [`totient_phi`].
///
/// This function uses an alternative method to calculate the $\phi(n)$.
/// In particular, for a given number $n \in \mathbb{N}$, we can find its unique prime
/// factorization, with their multiplicities, and calculate as follows.
/// Let $\n \in \mathbb{N}$. Then $n$ has a prime factorization of the form:
/// $$
///   n = p_1^{m_1} p_2^{m_2} p_3^{m_3} \cdots
/// $$
/// where the $p_i$'s are primes, and the $m_i$'s are positive multiplicities.
/// (Note: we can assume that all the $m_i$'s are non-zeroes, as those primes whose multiplicities
/// are zero will simply evaluate to $1$, and will be insignificant to us in this context.)
///
/// Then, $\phi(n)$ can be calculated as:
/// $$
///   \phi(n) = \prod_i (p_i - 1) p_i^{m_i - 1}
/// $$
pub fn totient_phi_alt(n: u64) -> u64 {
    get_prime_factor_multiplicities(n)
        .iter()
        .fold(1, |acc, (prime, multiplicity)| {
            acc * ((prime - 1) * prime.pow((multiplicity - 1) as u32))
        })
}

#[cfg(test)]
mod test {
    use super::{totient_phi, totient_phi_alt};

    #[test]
    fn check_phis() {
        assert_eq!(totient_phi(10), 4);
        assert_eq!(totient_phi(1), 1);
        assert_eq!(totient_phi(5), 4);
    }

    #[test]
    fn check_phis_alt() {
        assert_eq!(totient_phi_alt(10), 4);
        assert_eq!(totient_phi_alt(1), 1);
        assert_eq!(totient_phi_alt(5), 4);
    }
}
