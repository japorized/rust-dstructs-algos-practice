use crate::math::euclidean_algorithm::gcd;

/// Find the least common multiple of two numbers $a$ and $b$.
///
/// # Definition
///
/// ## Least Common Multiple
///
/// Given $a, b \in \mathbb{N}_{\geq 0}$, the least common multiple $d$ of the two numbers is
/// defined as the smallest possible positive integer that can be divided by both $a$ and $b$.
///
/// ---
///
/// # Formula
/// We'll use the following formula:
/// $$
///   \frac{ab}{\text{gcd}(a, b)}
/// $$
pub fn lcm(a: u64, b: u64) -> u64 {
    if a == 0 && b == 0 {
        0
    } else {
        (a / gcd(a, b)) * b
    }
}

#[cfg(test)]
mod test {
    use super::lcm;

    #[test]
    fn find_least_common_mulitple() {
        assert_eq!(lcm(0, 0), 0);
        assert_eq!(lcm(1, 0), 0);
        assert_eq!(lcm(1, 3), 3);
        assert_eq!(lcm(5, 3), 15);
        assert_eq!(lcm(4, 6), 12);
        assert_eq!(lcm(21, 6), 42);
    }
}
