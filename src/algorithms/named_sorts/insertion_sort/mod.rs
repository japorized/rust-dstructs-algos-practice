pub fn insertion_sort<T>(items: &[T]) -> Vec<T>
where
    T: Ord + Clone,
{
    if items.is_empty() {
        return Vec::new();
    }

    let mut sorted: Vec<T> = Vec::new();
    for item in items {
        let mut not_inserted = true;
        for (i, sorted_item) in sorted.clone().iter().enumerate() {
            if item < sorted_item {
                sorted.insert(i, item.clone());
                not_inserted = false;
                break;
            }
        }
        if not_inserted {
            sorted.push(item.clone());
        }
    }

    sorted
}

#[cfg(test)]
mod test {
    use super::insertion_sort;

    #[test]
    fn test_insertion_sort() {
        assert_eq!(insertion_sort(&vec![1]), [1]);
        assert_eq!(insertion_sort(&vec![1, 2]), [1, 2]);
        assert_eq!(insertion_sort(&vec![2, 1]), [1, 2]);
        assert_eq!(insertion_sort(&vec![1, 2, 3]), [1, 2, 3]);
        assert_eq!(insertion_sort(&vec![3, 2, 1]), [1, 2, 3]);
        assert_eq!(insertion_sort(&vec![3, 1, 2]), [1, 2, 3]);
        assert_eq!(insertion_sort(&vec![3, 1, -1, 5, 7]), [-1, 1, 3, 5, 7]);
        assert_eq!(
            insertion_sort(&vec!["cbc", "bbc", "abc"]),
            ["abc", "bbc", "cbc"]
        );

        let v: Vec<usize> = Vec::new();
        let v_sorted: [usize; 0] = [];
        assert_eq!(insertion_sort(&v), v_sorted);
    }
}
