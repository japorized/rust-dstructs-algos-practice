pub fn bubble_sort<'a, T>(items: &[&'a T]) -> Vec<&'a T>
where
    T: Eq + Ord + std::fmt::Debug,
{
    let items_length = items.len();
    let mut sorted = items.to_owned();
    if items_length == 0 {
        return sorted;
    }
    let mut has_sort = true;
    let mut counter = 1;
    while has_sort {
        has_sort = false;
        for i in 0..(items_length - counter) {
            let item1 = sorted
                .get(i)
                .unwrap_or_else(|| panic!("{}", format!("Could not get item {}", i)));
            let item2 = sorted
                .get(i + 1)
                .unwrap_or_else(|| panic!("{}", format!("Could not get item {}", i)));

            if item1 > item2 {
                sorted.swap(i, i + 1);
                has_sort = true;
            }
        }
        counter += 1;
    }

    sorted
}

#[cfg(test)]
mod test {
    use super::bubble_sort;

    #[test]
    fn test_bubble_sort() {
        assert_eq!(bubble_sort(&vec![&1]), &[&1]);
        assert_eq!(bubble_sort(&vec![&1, &2]), &[&1, &2]);
        assert_eq!(bubble_sort(&vec![&2, &1]), &[&1, &2]);
        assert_eq!(bubble_sort(&vec![&1, &2, &3]), &[&1, &2, &3]);
        assert_eq!(bubble_sort(&vec![&3, &2, &1]), &[&1, &2, &3]);
        assert_eq!(bubble_sort(&vec![&3, &1, &2]), &[&1, &2, &3]);
        assert_eq!(
            bubble_sort(&vec![&"cbc", &"bbc", &"abc"]),
            &[&"abc", &"bbc", &"cbc"]
        );

        let v: Vec<&usize> = Vec::new();
        let v_sorted: [&usize; 0] = [];
        assert_eq!(bubble_sort(&v), &v_sorted);
    }
}
