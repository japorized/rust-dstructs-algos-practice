use std::cmp::Ordering;

pub fn search_for_index<T>(items: &[T], item: &T) -> Option<usize>
where
    T: Eq + Ord + std::fmt::Debug,
{
    let items_length = items.len();

    if items_length == 1 {
        return match items.first() {
            Some(only_item) => {
                if only_item == item {
                    return Some(0);
                } else {
                    return None;
                }
            }
            None => None,
        };
    }

    let halfway_point = if items_length % 2 == 0 {
        items_length / 2
    } else {
        ((items_length + 1) / 2) - 1
    };

    match items.get(halfway_point) {
        Some(halfway_item) => match halfway_item.cmp(item) {
            Ordering::Equal => Some(halfway_point),
            Ordering::Greater => search_for_index(
                items
                    .get(0..halfway_point)
                    .expect("Could not slice the items vec"),
                item,
            ),
            Ordering::Less => search_for_index(
                items
                    .get(halfway_point..items_length)
                    .expect("Could not slice the items vec"),
                item,
            )
            .map(|index| index + halfway_point),
        },
        None => None,
    }
}

#[cfg(test)]
mod test {
    use super::search_for_index;

    #[test]
    fn test_get_index_through_binary_search() {
        assert_eq!(search_for_index(&vec![], &7), None);
        assert_eq!(search_for_index(&vec![1], &3), None);
        assert_eq!(search_for_index(&vec![1], &1), Some(0));
        assert_eq!(search_for_index(&vec![1, 2], &1), Some(0));
        assert_eq!(search_for_index(&vec![1, 2], &2), Some(1));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5], &7), None);
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5], &3), Some(2));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5], &2), Some(1));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5], &5), Some(4));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5, 6], &7), None);
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5, 6], &4), Some(3));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5, 6], &1), Some(0));
        assert_eq!(search_for_index(&vec![1, 2, 3, 4, 5, 6], &6), Some(5));
        assert_eq!(
            search_for_index(&vec!["abc", "bbc", "cbc"], &"cbc"),
            Some(2)
        );
    }
}
