use std::mem;

// Since we're in Rust land here, might as well take advantage of the `Ord` trait. We shall always
// assume that the vec of elements that we get are from a totally ordered set.

/// Get the strictly second largest element from the vec.
/// By strict, we mean that we treat items that are of equal value as the same item.
/// E.g. we expect that the following:
/// ```
/// use lib::algorithms::find_second_largest::get_second_largest_strict;
/// assert_eq!(get_second_largest_strict(&vec![1, 2, 2]), Some(&1));
/// assert_ne!(get_second_largest_strict(&vec![1, 2, 2]), Some(&2));
/// ```
/// This is different from that of [`get_second_largest_loose`].
pub fn get_second_largest_strict<T>(items: &[T]) -> Option<&T>
where
    T: Ord,
{
    if items.len() < 2 {
        return None;
    }

    let mut index_of_largest = 0;
    let mut index_of_second_largest = 1;

    if items[index_of_second_largest] > items[index_of_largest] {
        mem::swap(&mut index_of_largest, &mut index_of_second_largest);
    }

    for i in 2..items.len() {
        let current_item = &items[i];
        if current_item > &items[index_of_largest] {
            index_of_second_largest = index_of_largest;
            index_of_largest = i;
        } else if current_item > &items[index_of_second_largest]
            && current_item != &items[index_of_largest]
        {
            index_of_second_largest = i;
        }
    }

    Some(&items[index_of_second_largest])
}

/// Get the second largest element from the vec.
/// Note that this treats all elements uniquely.
/// E.g. we expect that the following:
/// ```
/// use lib::algorithms::find_second_largest::get_second_largest_loose;
/// assert_eq!(get_second_largest_loose(&vec![1, 2, 2]), Some(&2));
/// assert_ne!(get_second_largest_loose(&vec![1, 2, 2]), Some(&1));
/// ```
/// This is different from that of [`get_second_largest_strict`].
pub fn get_second_largest_loose<T>(items: &[T]) -> Option<&T>
where
    T: Ord,
{
    if items.len() < 2 {
        return None;
    }

    let mut index_of_largest = 0;
    let mut index_of_second_largest = 1;

    if items[index_of_second_largest] > items[index_of_largest] {
        mem::swap(&mut index_of_largest, &mut index_of_second_largest);
    }

    for i in 2..items.len() {
        let current_item = &items[i];
        if current_item > &items[index_of_largest] && i != index_of_largest {
            index_of_second_largest = index_of_largest;
            index_of_largest = i;
        } else if current_item > &items[index_of_second_largest] && i != index_of_second_largest {
            index_of_second_largest = i;
        }
    }

    Some(&items[index_of_second_largest])
}

#[cfg(test)]
mod test {
    use super::{get_second_largest_loose, get_second_largest_strict};

    #[test]
    fn test_get_second_largest_strict() {
        assert_eq!(get_second_largest_strict(&vec![1]), None);
        assert_eq!(get_second_largest_strict(&vec![1, 2]), Some(&1));
        assert_eq!(get_second_largest_strict(&vec![1, 2, 3]), Some(&2));
        assert_eq!(get_second_largest_strict(&vec![3, 2, 1]), Some(&2));
        assert_eq!(get_second_largest_strict(&vec![1, 3, 3]), Some(&1));
        assert_eq!(get_second_largest_strict(&vec![4, 7, 1, 9, 3]), Some(&7));
        assert_eq!(get_second_largest_strict(&vec![4, 7, 9, 9, 7]), Some(&7));

        assert_eq!(
            get_second_largest_strict(&vec!["abc", "bbc", "cbc"]),
            Some(&"bbc")
        );
    }

    #[test]
    fn test_get_second_largest_loose() {
        assert_eq!(get_second_largest_strict(&vec![1]), None);
        assert_eq!(get_second_largest_loose(&vec![1, 2]), Some(&1));
        assert_eq!(get_second_largest_loose(&vec![1, 2, 3]), Some(&2));
        assert_eq!(get_second_largest_loose(&vec![3, 2, 1]), Some(&2));
        assert_eq!(get_second_largest_loose(&vec![1, 3, 3]), Some(&3));
        assert_eq!(get_second_largest_loose(&vec![4, 7, 1, 9, 3]), Some(&7));
        assert_eq!(get_second_largest_loose(&vec![4, 7, 9, 9, 7]), Some(&9));

        assert_eq!(
            get_second_largest_loose(&vec!["abc", "bbc", "cbc"]),
            Some(&"bbc")
        );
    }
}
