# Find second largest number

The requirement is simple: find the second largest number in a given array.

Funnily enough, during interviews, you'd get answers that try to do something along the lines of:
1. Get the largest.
2. Remove the largest from original array.
3. Get the largest from the original array.

So much that it's kind of a meme now.
