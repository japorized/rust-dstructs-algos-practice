use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;

use crate::data_structures::graphs::weighted_graph::WeightedGraph;
use crate::data_structures::priority_queue::PriorityQueue;

pub fn dijkstra<T>(
    graph: WeightedGraph<T>,
    starting_node: T,
) -> (HashMap<T, i32>, HashMap<T, Option<T>>)
where
    T: Eq + Hash + Copy + Debug,
{
    let mut pq = PriorityQueue::<T>::new();
    let mut distances_from_start = HashMap::<T, i32>::new();
    let mut closest_node_to_start = HashMap::<T, Option<T>>::new();

    distances_from_start.insert(starting_node, 0);
    pq.insert(starting_node, 0);

    for node in graph.nodes.keys() {
        if *node != starting_node {
            closest_node_to_start.insert(*node, None);
            distances_from_start.insert(*node, i32::MAX);
            pq.insert(*node, i32::MAX);
        }
    }

    while let Some(node) = pq.pop() {
        for (neighbour, weight_to_neighbour) in graph.get_neighbours_of(node) {
            let distance = distances_from_start[&node] + weight_to_neighbour;
            if distance < distances_from_start[&neighbour] {
                closest_node_to_start.insert(neighbour, Some(node));
                distances_from_start.insert(neighbour, distance);

                pq.insert(neighbour, distance);
            }
        }
    }

    (distances_from_start, closest_node_to_start)
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;

    use crate::data_structures::graphs::weighted_graph::WeightedGraph;

    use super::dijkstra;

    #[test]
    fn check_dijkstra_case_1() {
        let mut g = WeightedGraph::new();
        g.add_edge(0, 1, 2);
        g.add_edge(0, 2, 6);
        g.add_edge(1, 3, 5);
        g.add_edge(2, 3, 8);
        g.add_edge(3, 4, 10);
        g.add_edge(3, 5, 15);
        g.add_edge(4, 5, 6);
        g.add_edge(4, 6, 2);
        g.add_edge(5, 6, 6);

        let (actual_distances_from_start, actual_closest_node_to_start) = dijkstra(g, 0);
        let expected_distances_from_start =
            HashMap::<i32, i32>::from([(0, 0), (1, 2), (2, 6), (3, 7), (4, 17), (5, 22), (6, 19)]);
        let expected_closest_node_to_start =
            HashMap::<i32, i32>::from([(1, 0), (2, 0), (3, 1), (4, 3), (5, 3), (6, 4)]);

        assert_eq!(actual_distances_from_start, expected_distances_from_start);
        assert_eq!(
            actual_closest_node_to_start.len(),
            expected_closest_node_to_start.len()
        );

        for (k, expected_node) in expected_closest_node_to_start {
            let actual_maybe = actual_closest_node_to_start.get(&k);
            assert!(actual_maybe.is_some());
            if let Some(actual) = actual_maybe {
                if let Some(actual_node) = actual {
                    assert_eq!(*actual_node, expected_node);
                }
            }
        }
    }

    // This is a long enough case that not all distances will be filled, and the algorithm is
    // expected to not find out through which node would it be closest to the starting node.
    // Thus, we'll only check for items that we know are on the minimal path.
    #[test]
    fn check_dijkstra_case_2() {
        let mut g = WeightedGraph::new();
        g.add_edge('S', 'A', 7);
        g.add_edge('S', 'B', 2);
        g.add_edge('S', 'C', 3);

        g.add_edge('B', 'A', 3);
        g.add_edge('B', 'H', 1);
        g.add_edge('A', 'D', 4);
        g.add_edge('D', 'F', 5);
        g.add_edge('F', 'H', 3);
        g.add_edge('H', 'G', 2);
        g.add_edge('G', 'E', 2);

        g.add_edge('C', 'L', 2);
        g.add_edge('L', 'I', 4);
        g.add_edge('I', 'K', 4);
        g.add_edge('K', 'E', 5);

        let (actual_distances_from_start, actual_closest_node_to_start) = dijkstra(g, 'S');
        let expected_distances_from_start =
            HashMap::<char, i32>::from([('S', 0), ('B', 2), ('E', 7), ('G', 5), ('H', 3)]);
        let expected_closest_node_to_start =
            HashMap::<char, char>::from([('B', 'S'), ('E', 'G'), ('G', 'H'), ('H', 'B')]);

        for (k, expected_distance) in expected_distances_from_start {
            let actual_maybe = actual_distances_from_start.get(&k);
            assert!(actual_maybe.is_some());
            if let Some(actual) = actual_maybe.copied() {
                assert_eq!(actual, expected_distance);
            }
        }

        for (k, expected_node) in expected_closest_node_to_start {
            let actual_maybe = actual_closest_node_to_start.get(&k);
            if let Some(actual) = actual_maybe {
                assert_eq!(*actual, Some(expected_node));
            }
        }
    }
}
