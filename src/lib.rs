//! # Data Structures, Algorithms, and Math!
//! This is a project where various data structures. algorithms, and mathematical concepts and
//! objects are implemented, for the sake of research and learning.
//!
//! ## Disclaimers
//! Here's the usual disclaimer when it comes to learning/research materials:
//!
//! > **This is not meant to be used in production**
//!
//! Here's my own disclaimer:
//!
//! > I am very much a budding learner who learns at my own pace. Whatever that is written here is
//! > more so to try things out, implement well-known data structures and algorithms, and not
//! > necessarily doing them *The Right Way*™. I am very much still learning what's considered
//! > idiomatic to Rust.
//!
//! ## References
//!
//! The inspiration of this project began from one that was made for [JavaScript](https://github.com/trekhleb/javascript-algorithms).
//!
//! Here are some of my question banks:
//! - The project that inspired the creation of this project itself (see above)
//! - [99 Haskell Problems](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems)

/// This "module" contains practice implementations for various algorithms
pub mod algorithms;

/// This "module" contains practice implementations for various data structures
pub mod data_structures;

/// This "module" contains practice implementations for various math concepts and objects
pub mod math;
