/// A queue where each value is given an integer priority.
///
/// You can use a `PriorityQueue` in a `for` loop, and items will be looped in order of priority,
/// from low to high priority.
use std::collections::HashMap;

use super::queue::Queue;

pub struct PriorityQueue<T> {
    items: HashMap<i32, T>,
}

impl<T> Default for PriorityQueue<T> {
    fn default() -> Self {
        Self {
            items: HashMap::new(),
        }
    }
}

impl<T> PriorityQueue<T> {
    pub fn new() -> Self {
        Self::default()
    }

    /// Inserts an item into the priority queue
    ///
    /// If the queue did not have an item with the given priority, None is returned.
    ///
    /// Otherwise, the existing item which had that priority will be returned, and the new item
    /// will takes its place in the queue.
    pub fn insert(&mut self, item: T, priority: i32) -> Option<T> {
        self.items.insert(priority, item)
    }

    /// Removes the item at a given priority
    ///
    /// If nothing in the queue holds that priority, None is returned.
    ///
    /// Otherwise, the item is returned.
    pub fn remove(&mut self, priority: i32) -> Option<T> {
        self.items.remove(&priority)
    }

    pub fn get(&self, priority: i32) -> Option<&T> {
        self.items.get(&priority)
    }

    pub fn pop(&mut self) -> Option<T> {
        let mut keys = self.items.keys().copied().collect::<Vec<i32>>();
        keys.sort_unstable();
        match keys.first().copied() {
            Some(k) => self.remove(k),
            None => None,
        }
    }
}

impl<T, const N: usize> From<[(i32, T); N]> for PriorityQueue<T> {
    fn from(value: [(i32, T); N]) -> Self {
        Self {
            items: HashMap::from(value),
        }
    }
}

impl<T> FromIterator<(i32, T)> for PriorityQueue<T> {
    fn from_iter<S: IntoIterator<Item = (i32, T)>>(iter: S) -> Self {
        Self {
            items: HashMap::from_iter(iter),
        }
    }
}

impl<T> IntoIterator for PriorityQueue<T> {
    type Item = T;

    type IntoIter = PriorityQueueIterator<T>;

    fn into_iter(self) -> Self::IntoIter {
        PriorityQueueIterator::new(self)
    }
}

pub struct PriorityQueueIterator<T> {
    queue: PriorityQueue<T>,
    priorities: Queue<i32>,
    cur_priority: Option<i32>,
}

impl<T> PriorityQueueIterator<T> {
    fn new(queue: PriorityQueue<T>) -> Self {
        let mut queue_keys = queue.items.keys().clone().copied().collect::<Vec<i32>>();
        queue_keys.sort_unstable();
        let mut priorities_queue = Queue::from(&queue_keys);
        let cur_priority = priorities_queue.dequeue();

        Self {
            queue,
            priorities: priorities_queue,
            cur_priority,
        }
    }
}

impl<T> Iterator for PriorityQueueIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.queue.items.is_empty() {
            return None;
        }

        match self.cur_priority {
            Some(p) => {
                if let Some(item) = self.queue.remove(p) {
                    self.cur_priority = self.priorities.dequeue();

                    Some(item)
                } else {
                    None
                }
            }
            None => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::PriorityQueue;

    #[test]
    fn can_queue_items_and_allows_iterating_over_them_in_priority_order() {
        let q = PriorityQueue::from([(5, 'e'), (3, 'c'), (12, 'l'), (1, 'a')]);

        let mut iter = q.into_iter();
        assert_eq!(iter.next(), Some('a'));
        assert_eq!(iter.next(), Some('c'));
        assert_eq!(iter.next(), Some('e'));
        assert_eq!(iter.next(), Some('l'));
        assert!(iter.next().is_none());
    }
}
