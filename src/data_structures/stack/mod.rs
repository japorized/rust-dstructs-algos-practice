pub struct Stack<T: Clone> {
    values: Vec<T>,
}

impl<T> Stack<T>
where
    T: Clone,
{
    pub fn new() -> Self {
        Stack { values: vec![] }
    }

    pub fn from(values: &[T]) -> Self {
        Stack {
            values: values.to_vec(),
        }
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn is_empty(&self) -> bool {
        self.values.is_empty()
    }

    pub fn push(&mut self, item: &T) {
        self.values.push(item.clone());
    }

    pub fn pop(&mut self) -> Option<T> {
        self.values.pop()
    }

    pub fn peek(&self) -> Option<&T> {
        match self.len() {
            0 => None,
            _ => self.values.get(self.len() - 1),
        }
    }

    pub fn clear(&mut self) {
        while self.peek().is_some() {
            self.pop();
        }
    }
}

impl<T> Default for Stack<T>
where
    T: Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use super::Stack;

    #[test]
    fn basic_stack_operations() {
        let mut stack1 = Stack::new();
        assert!(stack1.is_empty());
        assert_eq!(stack1.len(), 0);
        stack1.push(&1);
        assert!(!stack1.is_empty());
        assert_eq!(stack1.len(), 1);
        assert_eq!(stack1.peek(), Some(&1));
        stack1.push(&2);
        stack1.push(&3);
        assert_eq!(stack1.len(), 3);
        assert_eq!(stack1.peek(), Some(&3));
        assert_eq!(stack1.pop(), Some(3));
        assert_eq!(stack1.len(), 2);
        stack1.clear();
        assert!(stack1.is_empty());
        assert_eq!(stack1.len(), 0);
        assert_eq!(stack1.peek(), None);

        let stack2 = Stack::from(&vec![1, 2, 3]);
        assert_eq!(stack2.len(), 3);
        assert_eq!(stack2.peek(), Some(&3));
    }
}
