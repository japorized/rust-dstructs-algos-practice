pub struct Queue<T>
where
    T: Copy,
{
    values: Vec<T>,
}

impl<T> Default for Queue<T>
where
    T: Copy,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Queue<T>
where
    T: Copy,
{
    pub fn new() -> Self {
        Queue { values: vec![] }
    }

    pub fn from(starting_items: &[T]) -> Self {
        Queue {
            values: starting_items.to_vec(),
        }
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    pub fn is_empty(&self) -> bool {
        self.values.is_empty()
    }

    pub fn enqueue(&mut self, item: T) {
        self.values.push(item);
    }

    pub fn dequeue(&mut self) -> Option<T> {
        match self.values.len() {
            0 => None,
            1 => {
                let head = self.values[0];
                self.values = vec![];
                Some(head)
            }
            _ => {
                let head = self.values[0];
                self.values = self.values[1..].to_vec();
                Some(head)
            }
        }
    }

    pub fn peek(&self) -> Option<&T> {
        self.values.first()
    }

    pub fn push(&mut self, item: T) {
        self.enqueue(item);
    }
}

impl<T> FromIterator<T> for Queue<T>
where
    T: Copy,
{
    fn from_iter<S: IntoIterator<Item = T>>(iter: S) -> Self {
        let mut q = Self::default();
        for item in iter.into_iter() {
            q.push(item);
        }

        q
    }
}

#[cfg(test)]
mod test {
    use super::Queue;

    #[test]
    fn basic_queue_operations() {
        let mut queue1 = Queue::<usize>::new();
        assert!(queue1.is_empty());
        queue1.enqueue(1);
        assert_eq!(queue1.len(), 1);
        assert_eq!(queue1.peek(), Some(&1));
        queue1.push(2);
        assert_eq!(queue1.len(), 2);
        queue1.dequeue();
        assert_eq!(queue1.peek(), Some(&2));
        queue1.dequeue();
        assert!(queue1.is_empty());
        assert_eq!(queue1.peek(), None);

        let queue2 = Queue::from(&vec![3, 2, 1]);
        assert_eq!(queue2.len(), 3);
        assert_eq!(queue2.peek(), Some(&3));
    }
}
