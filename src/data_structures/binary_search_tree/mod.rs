mod node;

pub use node::BinarySearchTreeNode;
use std::{
    cmp::Ordering,
    fmt::{self, Display},
};

#[derive(Debug)]
pub struct BinarySearchTree<T: Ord + Copy + Display> {
    pub root_node: Option<Box<BinarySearchTreeNode<T>>>,
}

impl<T> BinarySearchTree<T>
where
    T: Ord + Copy + Display,
{
    pub fn new() -> Self {
        BinarySearchTree { root_node: None }
    }

    pub fn from(elems: &[T]) -> Self {
        let mut bst = BinarySearchTree::new();
        for elem in elems {
            bst.insert(*elem);
        }

        bst
    }

    pub fn insert(&mut self, value: T) {
        if self.root_node.is_some() {
            let root_node = self
                .root_node
                .as_mut()
                .expect("Could not unwrap root_node as mut");
            match root_node.value.cmp(&value) {
                Ordering::Equal => (),
                _ => root_node.insert(value),
            }
        } else {
            self.root_node = Some(Box::new(BinarySearchTreeNode::new(value)));
        }
    }

    pub fn contains(&self, value: T) -> bool {
        match &self.root_node {
            Some(root_node) => root_node.contains(value),
            None => false,
        }
    }

    pub fn count(&self) -> usize {
        match &self.root_node {
            Some(root_node) => root_node.count(),
            None => 0,
        }
    }

    pub fn depth(&self) -> usize {
        match &self.root_node {
            Some(root_node) => root_node.depth(),
            None => 0,
        }
    }

    pub fn delete(&mut self, value: T) -> bool {
        if self.root_node.is_some() {
            let root_node = self
                .root_node
                .as_mut()
                .expect("Could not unwrap root_node as mut");
            if root_node.value == value {
                self.root_node = None;

                true
            } else {
                root_node.delete_node(value)
            }
        } else {
            false
        }
    }

    pub fn find_parent_of(&self, value: T) -> Option<&BinarySearchTreeNode<T>> {
        match &self.root_node {
            Some(root_node) => {
                if root_node.value == value {
                    None
                } else {
                    root_node.find_parent_of(value)
                }
            }
            None => None,
        }
    }

    pub fn get_node_by_value(&self, value: T) -> Option<&BinarySearchTreeNode<T>> {
        match &self.root_node {
            Some(root_node) => root_node.get_node_by_value(value),
            None => None,
        }
    }

    pub fn find_min(&self) -> Option<T> {
        self.root_node
            .as_ref()
            .map(|root_node| root_node.find_min())
    }

    pub fn find_max(&self) -> Option<T> {
        self.root_node
            .as_ref()
            .map(|root_node| root_node.find_max())
    }
}

impl<T> Default for BinarySearchTree<T>
where
    T: Ord + Copy + Display,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Display for BinarySearchTree<T>
where
    T: Ord + Copy + Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let root_node_display = if let Some(root_node) = &self.root_node {
            root_node.to_string()
        } else {
            "None".to_string()
        };
        write!(f, "{}", root_node_display)
    }
}

#[cfg(test)]
mod test {
    use super::BinarySearchTree;

    #[test]
    fn contains() {
        let bst = BinarySearchTree::from(&vec![4, 3, 6, 1, 5, 10, 9, -2]);

        assert!(bst.contains(6));
        assert!(bst.contains(1));
        assert!(bst.contains(4));
        assert!(!bst.contains(-1));
    }

    #[test]
    fn count() {
        let bst = BinarySearchTree::<usize>::new();
        assert_eq!(bst.count(), 0);

        let vec1 = vec![3, 2, 4, 5];
        let bst1 = BinarySearchTree::from(&vec1);
        assert_eq!(bst1.count(), vec1.len());

        let vec2 = vec![-1, 6, 2, 10, 99];
        let bst2 = BinarySearchTree::from(&vec2);
        assert_eq!(bst2.count(), vec2.len());
    }

    #[test]
    fn depth() {
        let bst = BinarySearchTree::<usize>::new();
        assert_eq!(bst.depth(), 0);

        let bst1 = BinarySearchTree::from(&[3, 2, 4, 5]);
        assert_eq!(bst1.depth(), 3);

        let bst2 = BinarySearchTree::from(&[-1, 6, 2, 10, 99]);
        assert_eq!(bst2.depth(), 4);
    }

    #[test]
    fn delete() {
        let mut bst = BinarySearchTree::from(&vec![4, 3, 8, 10, -1]);
        assert_eq!(bst.count(), 5);
        assert!(bst.contains(10));

        bst.delete(10);
        assert_eq!(bst.count(), 4);
        assert!(!bst.contains(10));
    }

    #[test]
    fn find_parent_of() {
        let mut bst = BinarySearchTree::from(&vec![4, 3, 8, 10, -1]);
        assert_eq!(bst.find_parent_of(10).unwrap().value, 8);
        assert_eq!(bst.find_parent_of(8).unwrap().value, 4);

        bst.delete(8);
        assert_eq!(bst.find_parent_of(10), None);
    }

    #[test]
    fn get_node_by_value() {
        let mut bst = BinarySearchTree::from(&vec![4, 3, 8, 10, -1]);
        assert_eq!(bst.get_node_by_value(-2), None);
        assert_eq!(bst.get_node_by_value(10).unwrap().value, 10);

        bst.delete(10);
        assert_eq!(bst.get_node_by_value(10), None);
    }

    #[test]
    fn find_min() {
        assert_eq!(
            BinarySearchTree::from(&vec![4, 3, 8, 10, -1]).find_min(),
            Some(-1)
        );
        assert_eq!(BinarySearchTree::<usize>::from(&vec![]).find_min(), None);
    }

    #[test]
    fn find_max() {
        assert_eq!(
            BinarySearchTree::from(&vec![4, 3, 8, 10, -1]).find_max(),
            Some(10)
        );
        assert_eq!(BinarySearchTree::<usize>::from(&vec![]).find_max(), None);
    }
}
