use std::{
    cmp::{self, Ordering, PartialEq, PartialOrd},
    fmt::{self, Display},
};

#[derive(Debug)]
pub struct BinarySearchTreeNode<T: Ord + Copy + Display> {
    pub value: T,
    pub left_node: Option<Box<BinarySearchTreeNode<T>>>,
    pub right_node: Option<Box<BinarySearchTreeNode<T>>>,
}

impl<T> BinarySearchTreeNode<T>
where
    T: Ord + Copy + Display,
{
    pub fn new(value: T) -> Self {
        BinarySearchTreeNode {
            value,
            left_node: None,
            right_node: None,
        }
    }

    pub fn insert(&mut self, value: T) {
        // The ordering tells if self.value is less/greater/equal to value
        // Note that
        // - if value > self.value, then value goes to the right.
        // - if value < self.value, then value goes to the left.
        // - if value == self.value, then we do nothing cause the value already exists
        match self.value.cmp(&value) {
            Ordering::Less => {
                if self.right_node.is_some() {
                    let right_node = self
                        .right_node
                        .as_mut()
                        .expect("Could not unwrap right_node as mutable");
                    right_node.insert(value);
                } else {
                    self.right_node = Some(Box::new(BinarySearchTreeNode::new(value)));
                }
            }
            Ordering::Greater => {
                if self.left_node.is_some() {
                    let left_node = self
                        .left_node
                        .as_mut()
                        .expect("Could not unwrap left_node as mutable");
                    left_node.insert(value);
                } else {
                    self.left_node = Some(Box::new(BinarySearchTreeNode::new(value)));
                }
            }
            Ordering::Equal => (),
        }
    }

    pub fn contains(&self, value: T) -> bool {
        match self.value.cmp(&value) {
            Ordering::Less => match &self.right_node {
                Some(right_node) => right_node.contains(value),
                None => false,
            },
            Ordering::Greater => match &self.left_node {
                Some(left_node) => left_node.contains(value),
                None => false,
            },
            Ordering::Equal => true,
        }
    }

    pub fn count(&self) -> usize {
        // start by counting itself
        let mut count = 1;
        match &self.right_node {
            Some(right_node) => count += right_node.count(),
            None => (),
        };
        match &self.left_node {
            Some(left_node) => count += left_node.count(),
            None => (),
        }

        count
    }

    pub fn depth(&self) -> usize {
        let right_node_depth = match &self.right_node {
            Some(right_node) => right_node.depth(),
            None => 0,
        };
        let left_node_depth = match &self.left_node {
            Some(left_node) => left_node.depth(),
            None => 0,
        };

        cmp::max(left_node_depth, right_node_depth) + 1
    }

    /// Deletes the child node if their value matches the selection.
    ///
    /// Returns true if there was indeed a deleteion.
    pub fn delete_node(&mut self, value: T) -> bool {
        match &self.value.cmp(&value) {
            Ordering::Less => {
                if self.right_node.is_some() {
                    let right_node = self
                        .right_node
                        .as_mut()
                        .expect("Could not unwrap right_noed as mut");
                    return if right_node.value == value {
                        self.right_node = None;

                        true
                    } else {
                        right_node.delete_node(value)
                    };
                }

                false
            }
            Ordering::Greater => {
                if self.left_node.is_some() {
                    let left_node = self
                        .left_node
                        .as_mut()
                        .expect("Could not unwrap left_noed as mut");
                    return if left_node.value == value {
                        self.left_node = None;

                        true
                    } else {
                        left_node.delete_node(value)
                    };
                }

                false
            }
            Ordering::Equal => false,
        }
    }

    pub fn find_parent_of(&self, value: T) -> Option<&Self> {
        match &self.value.cmp(&value) {
            Ordering::Less => match &self.right_node {
                Some(right_node) => {
                    if right_node.value == value {
                        Some(self)
                    } else {
                        right_node.find_parent_of(value)
                    }
                }
                None => None,
            },
            Ordering::Greater => match &self.left_node {
                Some(left_node) => {
                    if left_node.value == value {
                        Some(self)
                    } else {
                        left_node.find_parent_of(value)
                    }
                }
                None => None,
            },
            Ordering::Equal => None,
        }
    }

    pub fn get_node_by_value(&self, value: T) -> Option<&Self> {
        match self.value.cmp(&value) {
            Ordering::Equal => Some(self),
            Ordering::Less => match &self.right_node {
                Some(right_node) => right_node.get_node_by_value(value),
                None => None,
            },
            Ordering::Greater => match &self.left_node {
                Some(left_node) => left_node.get_node_by_value(value),
                None => None,
            },
        }
    }

    pub fn find_min(&self) -> T {
        if let Some(left_node) = &self.left_node {
            left_node.find_min()
        } else {
            self.value
        }
    }

    pub fn find_max(&self) -> T {
        if let Some(right_node) = &self.right_node {
            right_node.find_max()
        } else {
            self.value
        }
    }
}

impl<T> Display for BinarySearchTreeNode<T>
where
    T: Ord + Copy + Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let left_node_display = if let Some(left_node) = &self.left_node {
            left_node.to_string()
        } else {
            "None".to_string()
        };
        let right_node_display = if let Some(right_node) = &self.right_node {
            right_node.to_string()
        } else {
            "None".to_string()
        };
        write!(
            f,
            "{{ {}: {{ left: {}, right: {} }} }}",
            self.value, left_node_display, right_node_display
        )
    }
}

impl<T> PartialEq for BinarySearchTreeNode<T>
where
    T: Ord + Copy + Display,
{
    fn eq(&self, other: &Self) -> bool {
        if self.value == other.value {
            self.left_node.eq(&other.left_node) && self.right_node.eq(&other.right_node)
        } else {
            false
        }
    }
}

impl<T> Eq for BinarySearchTreeNode<T> where T: Ord + Copy + Display {}

impl<T> PartialOrd for BinarySearchTreeNode<T>
where
    T: Ord + Copy + Display,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else if self.contains(other.value) {
            Some(Ordering::Greater)
        } else if other.contains(self.value) {
            Some(Ordering::Less)
        } else {
            None
        }
    }
}
