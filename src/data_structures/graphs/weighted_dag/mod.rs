use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::hash::Hash;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Node<T: Eq + Hash + Copy + Debug> {
    pub value: T,
}

#[derive(Copy, Clone, Debug)]
pub struct Edge<T: Eq + Hash + Copy + Debug> {
    pub from: T,
    pub to: T,
    pub weight: f64,
}

impl<T> PartialEq for Edge<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn eq(&self, other: &Self) -> bool {
        self.from == other.from && self.to == other.to
    }
}
impl<T> Eq for Edge<T> where T: Eq + Hash + Copy + Debug {}
impl<T> Hash for Edge<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.from.hash(state);
        self.to.hash(state);
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct WeightedDag<T: Eq + Hash + Copy + Debug> {
    pub nodes: HashMap<T, Node<T>>,
    pub edges: Vec<Edge<T>>,
}

impl<T> Default for WeightedDag<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn default() -> Self {
        WeightedDag {
            nodes: HashMap::new(),
            edges: Vec::new(),
        }
    }
}

impl<T> WeightedDag<T>
where
    T: Eq + Hash + Copy + Debug,
{
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_nodes(nodes: Vec<T>) -> Self {
        WeightedDag {
            nodes: HashMap::from_iter(
                nodes
                    .into_iter()
                    .map(|n| (n, Node { value: n }))
                    .collect::<Vec<(T, Node<T>)>>(),
            ),
            edges: Vec::new(),
        }
    }

    pub fn add_node(&mut self, node_value: T) -> bool {
        if self.nodes.contains_key(&node_value) {
            return false;
        }

        self.nodes.insert(node_value, Node { value: node_value });
        true
    }

    pub fn remove_node(&mut self, node_value: T) {
        self.edges = self
            .edges
            .clone()
            .into_iter()
            .filter(|e| !(e.from == node_value || e.to == node_value))
            .collect();

        self.nodes.remove(&node_value);
    }

    pub fn has_node(&self, node_value: T) -> bool {
        self.nodes.contains_key(&node_value)
    }

    pub fn get_root_nodes(&self) -> Vec<T> {
        self.nodes
            .clone()
            .into_iter()
            .filter_map(|(n, _)| match self.edges.iter().find(|e| e.to == n) {
                Some(_) => None,
                None => Some(n),
            })
            .collect()
    }

    pub fn get_children_of(&self, node_value: T) -> Vec<(T, f64)> {
        self.edges
            .clone()
            .into_iter()
            .filter_map(|e| {
                if e.from == node_value {
                    Some((e.to, e.weight))
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn get_connected_graphs(&self) -> Vec<Self> {
        if self.nodes.is_empty() {
            return Vec::new();
        }

        let mut connected_graphs = Vec::<Self>::new();
        let mut nodes: HashSet<T> = HashSet::from_iter(self.nodes.clone().keys().copied());

        let mut starting_node = nodes.iter().next().copied();
        let mut visited_nodes = HashSet::<T>::new();
        let mut related_edges = HashSet::<Edge<T>>::new();
        while let Some(node) = starting_node {
            Self::get_connected_graphs_len_step(self, &mut visited_nodes, &mut related_edges, node);
            let mut connected_graph = Self::with_nodes(visited_nodes.clone().into_iter().collect());
            for edge in related_edges.clone() {
                connected_graph.edges.push(edge);
            }
            connected_graphs.push(connected_graph);

            nodes = nodes
                .difference(&HashSet::from_iter(visited_nodes.clone().into_iter()))
                .copied()
                .collect();

            visited_nodes = HashSet::new();
            related_edges = HashSet::new();
            starting_node = nodes.iter().next().copied();
        }

        connected_graphs
    }

    fn get_connected_graphs_len_step(
        s: &Self,
        visited_nodes: &mut HashSet<T>,
        related_edges: &mut HashSet<Edge<T>>,
        starting_node: T,
    ) {
        if visited_nodes.contains(&starting_node) {
            return;
        }

        visited_nodes.insert(starting_node);
        let touched_edges: HashSet<Edge<T>> = s
            .edges
            .clone()
            .into_iter()
            .filter(|e| e.from == starting_node || e.to == starting_node)
            .collect();
        for e in touched_edges {
            related_edges.insert(e);
            let neighbour = if e.from == starting_node {
                e.to
            } else {
                e.from
            };
            Self::get_connected_graphs_len_step(s, visited_nodes, related_edges, neighbour);
        }
    }

    pub fn has_cycles(&self) -> bool {
        let connected_graphs = self.get_connected_graphs();
        if connected_graphs.is_empty() {
            return false;
        }

        for graph in connected_graphs {
            let roots = graph.get_root_nodes();
            // If the get_root_nodes method couldn't find any node that would be a root, and there are
            // nodes in the graph, we know that there must be some cycle.
            if roots.is_empty() && !graph.nodes.is_empty() {
                return true;
            }

            let mut visited = Vec::<T>::new();
            for root in roots {
                if Self::has_cycles_step(&graph, &mut visited, root) {
                    return true;
                }
            }
        }

        false
    }

    fn has_cycles_step(s: &Self, visited: &mut Vec<T>, node: T) -> bool {
        if visited.contains(&node) {
            return true;
        }

        visited.push(node);

        for child in s.get_children_of(node) {
            if Self::has_cycles_step(s, visited, child.0) {
                return true;
            }
        }

        false
    }

    pub fn add_edge(&mut self, from: T, to: T, weight: f64) -> bool {
        let edge = Edge { from, to, weight };
        if self.edges.contains(&edge) {
            return false;
        }

        let is_from_added = self.add_node(from);
        let is_to_added = self.add_node(to);
        self.edges.push(edge);

        if self.has_cycles() {
            if is_from_added {
                self.remove_node(from);
            }

            if is_to_added {
                self.remove_node(to);
            }

            self.remove_edge(from, to);
            return false;
        }

        true
    }

    pub fn remove_edge(&mut self, from: T, to: T) {
        self.edges = self
            .edges
            .clone()
            .into_iter()
            .filter(|e| !(e.from == from && e.to == to))
            .collect();
    }

    pub fn has_edge(&self, from: T, to: T) -> bool {
        self.edges.contains(&Edge {
            from,
            to,
            weight: 0.,
        })
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashSet;

    use super::*;

    #[test]
    fn can_add_nodes() {
        let mut g = WeightedDag::with_nodes(vec!["foo", "bar"]);
        assert!(g.has_node("foo"));
        assert!(g.has_node("bar"));
        assert!(!g.has_node("baz"));

        g.add_node("baz");
        assert!(g.has_node("baz"));
    }

    #[test]
    fn can_remove_nodes() {
        let mut g = WeightedDag::new();
        assert!(!g.has_node(1));

        g.add_node(1);
        assert!(g.has_node(1));

        g.remove_node(1);
        assert!(!g.has_node(1));

        // Make sure that removing a non-existing node doesn't panic
        g.remove_node(1);
    }

    #[test]
    fn can_identify_root_nodes() {
        let mut g = WeightedDag::with_nodes(vec![1, 2]);
        g.add_edge(1, 3, 1.);
        g.add_edge(2, 4, 0.);

        g.add_edge(5, 6, 0.);

        let roots = g.get_root_nodes();
        let expectation = HashSet::from([1, 2, 5]);
        let root_set = HashSet::<i32>::from_iter(roots.into_iter());
        assert_eq!(root_set, expectation);
    }

    #[test]
    fn can_add_edges() {
        let mut g = WeightedDag::new();
        g.add_edge(1, 2, 1.);
        assert!(g.has_edge(1, 2));
    }

    #[test]
    fn can_get_children_of_nodes() {
        let mut g = WeightedDag::new();
        g.add_edge(1, 2, 0.);
        g.add_edge(2, 3, 1.);
        g.add_edge(2, 4, 2.);

        let expectation = HashSet::from([2]);
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_children_of(1).into_iter().map(|c| c.0)),
            expectation
        );

        let expectation = HashSet::from([3, 4]);
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_children_of(2).into_iter().map(|c| c.0)),
            expectation
        );

        let expectation = HashSet::new();
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_children_of(4).into_iter().map(|c| c.0)),
            expectation
        );
    }

    #[test]
    fn cannot_add_edge_that_makes_a_cycle() {
        let mut g = WeightedDag::new();
        assert!(g.add_edge("a", "b", 1.));
        assert!(g.add_edge("b", "c", 2.));
        assert!(g.add_edge("c", "d", 3.));
        assert!(!g.add_edge("b", "a", 2.));
        assert!(!g.add_edge("c", "a", 3.));
        assert!(!g.add_edge("d", "a", 3.));

        assert_eq!(g.nodes.len(), 4);
        assert_eq!(g.edges.len(), 3);

        // Here's a case with 2 connected graphs
        let mut g2 = WeightedDag::with_nodes(vec![1]);
        g2.add_edge(2, 3, 0.);
        assert!(!g2.add_edge(3, 2, 0.));
    }

    #[test]
    fn can_show_connected_graphs() {
        let mut g = WeightedDag::with_nodes(vec![1, 2]);
        g.add_edge(1, 3, 0.);
        g.add_edge(2, 4, 0.);

        let connected_graphs = g.get_connected_graphs();
        assert_eq!(connected_graphs.len(), 2);
        let graph_with_one = connected_graphs
            .iter()
            .find(|graph| graph.nodes.contains_key(&1));
        assert!(graph_with_one.is_some());
        let nodes: Vec<i32> = graph_with_one.unwrap().nodes.keys().map(|n| *n).collect();
        let actual = HashSet::from_iter(nodes.into_iter());
        assert_eq!(actual, HashSet::<i32>::from_iter([1, 3]));

        let graph_with_two = connected_graphs
            .iter()
            .find(|graph| graph.nodes.contains_key(&2));
        assert!(graph_with_two.is_some());
        let nodes: Vec<i32> = graph_with_two.unwrap().nodes.keys().map(|n| *n).collect();
        let actual = HashSet::from_iter(nodes.into_iter());
        assert_eq!(actual, HashSet::<i32>::from_iter([2, 4]));
    }
}
