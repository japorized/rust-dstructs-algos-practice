use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::hash::Hash;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Node<T: Eq + Hash + Copy + Debug> {
    pub value: T,
}

#[derive(Copy, Clone, Debug)]
pub struct Edge<T: Eq + Hash + Copy + Debug> {
    pub a: T,
    pub b: T,
    pub weight: i32,
}

impl<T> PartialEq for Edge<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn eq(&self, other: &Self) -> bool {
        (self.a == other.a && self.b == other.b) || (self.a == other.b && self.b == other.a)
    }
}
impl<T> Eq for Edge<T> where T: Eq + Hash + Copy + Debug {}
impl<T> Hash for Edge<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.a.hash(state);
        self.b.hash(state);
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct WeightedGraph<T: Eq + Hash + Copy + Debug> {
    pub nodes: HashMap<T, Node<T>>,
    pub edges: Vec<Edge<T>>,
}

impl<T> Default for WeightedGraph<T>
where
    T: Eq + Hash + Copy + Debug,
{
    fn default() -> Self {
        WeightedGraph {
            nodes: HashMap::new(),
            edges: Vec::new(),
        }
    }
}

impl<T> WeightedGraph<T>
where
    T: Eq + Hash + Copy + Debug,
{
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_nodes(nodes: Vec<T>) -> Self {
        WeightedGraph {
            nodes: HashMap::from_iter(
                nodes
                    .into_iter()
                    .map(|n| (n, Node { value: n }))
                    .collect::<Vec<(T, Node<T>)>>(),
            ),
            edges: Vec::new(),
        }
    }

    pub fn add_node(&mut self, node_value: T) -> bool {
        if self.nodes.contains_key(&node_value) {
            return false;
        }

        self.nodes.insert(node_value, Node { value: node_value });
        true
    }

    pub fn remove_node(&mut self, node_value: T) {
        self.edges = self
            .edges
            .clone()
            .into_iter()
            .filter(|e| !(e.a == node_value || e.b == node_value))
            .collect();

        self.nodes.remove(&node_value);
    }

    pub fn has_node(&self, node_value: T) -> bool {
        self.nodes.contains_key(&node_value)
    }

    pub fn get_root_nodes(&self) -> Vec<T> {
        self.nodes
            .clone()
            .into_iter()
            .filter_map(|(n, _)| match self.edges.iter().find(|e| e.b == n) {
                Some(_) => None,
                None => Some(n),
            })
            .collect()
    }

    pub fn get_neighbours_of(&self, node_value: T) -> Vec<(T, i32)> {
        self.edges
            .clone()
            .into_iter()
            .filter_map(|e| {
                if e.a == node_value {
                    Some((e.b, e.weight))
                } else if e.b == node_value {
                    Some((e.a, e.weight))
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn get_connected_graphs(&self) -> Vec<Self> {
        if self.nodes.is_empty() {
            return Vec::new();
        }

        let mut connected_graphs = Vec::<Self>::new();
        let mut nodes: HashSet<T> = HashSet::from_iter(self.nodes.clone().keys().copied());

        let mut starting_node = nodes.iter().next().copied();
        let mut visited_nodes = HashSet::<T>::new();
        let mut related_edges = HashSet::<Edge<T>>::new();
        while let Some(node) = starting_node {
            Self::get_connected_graphs_len_step(self, &mut visited_nodes, &mut related_edges, node);
            let mut connected_graph = Self::with_nodes(visited_nodes.clone().into_iter().collect());
            for edge in related_edges.clone() {
                connected_graph.edges.push(edge);
            }
            connected_graphs.push(connected_graph);

            nodes = nodes
                .difference(&HashSet::from_iter(visited_nodes.clone().into_iter()))
                .copied()
                .collect();

            visited_nodes = HashSet::new();
            related_edges = HashSet::new();
            starting_node = nodes.iter().next().copied();
        }

        connected_graphs
    }

    fn get_connected_graphs_len_step(
        s: &Self,
        visited_nodes: &mut HashSet<T>,
        related_edges: &mut HashSet<Edge<T>>,
        starting_node: T,
    ) {
        if visited_nodes.contains(&starting_node) {
            return;
        }

        visited_nodes.insert(starting_node);
        let touched_edges: HashSet<Edge<T>> = s
            .edges
            .clone()
            .into_iter()
            .filter(|e| e.a == starting_node || e.b == starting_node)
            .collect();
        for e in touched_edges {
            related_edges.insert(e);
            let neighbour = if e.a == starting_node { e.b } else { e.a };
            Self::get_connected_graphs_len_step(s, visited_nodes, related_edges, neighbour);
        }
    }

    pub fn add_edge(&mut self, a: T, b: T, weight: i32) -> bool {
        let edge = Edge { a, b, weight };
        if self.edges.contains(&edge) {
            return false;
        }

        self.add_node(a);
        self.add_node(b);
        self.edges.push(edge);

        true
    }

    pub fn remove_edge(&mut self, a: T, b: T) {
        let edge = Edge { a, b, weight: 0 };
        self.edges = self
            .edges
            .clone()
            .into_iter()
            .filter(|e| *e != edge)
            .collect();
    }

    pub fn has_edge(&self, a: T, b: T) -> bool {
        self.edges.contains(&Edge { a, b, weight: 0 })
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashSet;

    use super::*;

    #[test]
    fn can_add_nodes() {
        let mut g = WeightedGraph::with_nodes(vec!["foo", "bar"]);
        assert!(g.has_node("foo"));
        assert!(g.has_node("bar"));
        assert!(!g.has_node("baz"));

        g.add_node("baz");
        assert!(g.has_node("baz"));
    }

    #[test]
    fn can_remove_nodes() {
        let mut g = WeightedGraph::new();
        assert!(!g.has_node(1));

        g.add_node(1);
        assert!(g.has_node(1));

        g.remove_node(1);
        assert!(!g.has_node(1));

        // Make sure that removing a non-existing node doesn't panic
        g.remove_node(1);
    }

    #[test]
    fn can_identify_root_nodes() {
        let mut g = WeightedGraph::with_nodes(vec![1, 2]);
        g.add_edge(1, 3, 1);
        g.add_edge(2, 4, 0);

        g.add_edge(5, 6, 0);

        let roots = g.get_root_nodes();
        let expectation = HashSet::from([1, 2, 5]);
        let root_set = HashSet::<i32>::from_iter(roots.into_iter());
        assert_eq!(root_set, expectation);
    }

    #[test]
    fn can_add_edges() {
        let mut g = WeightedGraph::new();
        g.add_edge(1, 2, 1);
        assert!(g.has_edge(1, 2));
    }

    #[test]
    fn can_get_neighbours_of_nodes() {
        let mut g = WeightedGraph::new();
        g.add_edge(1, 2, 0);
        g.add_edge(2, 3, 1);
        g.add_edge(2, 4, 2);

        let expectation = HashSet::from([2]);
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_neighbours_of(1).into_iter().map(|c| c.0)),
            expectation
        );

        let expectation = HashSet::from([1, 3, 4]);
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_neighbours_of(2).into_iter().map(|c| c.0)),
            expectation
        );

        let expectation = HashSet::from([2]);
        assert_eq!(
            HashSet::<i32>::from_iter(g.get_neighbours_of(4).into_iter().map(|c| c.0)),
            expectation
        );
    }

    #[test]
    fn can_show_connected_graphs() {
        let mut g = WeightedGraph::with_nodes(vec![1, 2]);
        g.add_edge(1, 3, 0);
        g.add_edge(2, 4, 0);

        let connected_graphs = g.get_connected_graphs();
        assert_eq!(connected_graphs.len(), 2);
        let graph_with_one = connected_graphs
            .iter()
            .find(|graph| graph.nodes.contains_key(&1));
        assert!(graph_with_one.is_some());
        let nodes: Vec<i32> = graph_with_one.unwrap().nodes.keys().map(|n| *n).collect();
        let actual = HashSet::from_iter(nodes.into_iter());
        assert_eq!(actual, HashSet::<i32>::from_iter([1, 3]));

        let graph_with_two = connected_graphs
            .iter()
            .find(|graph| graph.nodes.contains_key(&2));
        assert!(graph_with_two.is_some());
        let nodes: Vec<i32> = graph_with_two.unwrap().nodes.keys().map(|n| *n).collect();
        let actual = HashSet::from_iter(nodes.into_iter());
        assert_eq!(actual, HashSet::<i32>::from_iter([2, 4]));
    }
}
