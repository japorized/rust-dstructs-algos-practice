use std::cell::RefCell;
use std::rc::Rc;

#[derive(Clone, Debug)]
pub struct Node<T: Copy + Eq> {
    pub value: T,
    pub next_node: Option<Rc<RefCell<Node<T>>>>,
}

impl<T> Node<T>
where
    T: Copy + Eq,
{
    pub fn new(value: T) -> Self {
        Node {
            value,
            next_node: None,
        }
    }

    pub fn append_node(&mut self, new_node_ref: Rc<RefCell<Node<T>>>) {
        if let Some(next_node) = &self.next_node {
            next_node.borrow_mut().append_node(new_node_ref);
        } else {
            self.next_node = Some(Rc::clone(&new_node_ref));
        }
    }

    pub fn append(&mut self, value: T) {
        if let Some(next_node) = &self.next_node {
            next_node.borrow_mut().append(value);
        } else {
            self.next_node = Some(Rc::new(RefCell::new(Node::new(value))));
        }
    }

    pub fn contains(&self, value: T) -> bool {
        if self.value == value {
            true
        } else if let Some(next_node) = &self.next_node {
            next_node.borrow().contains(value)
        } else {
            false
        }
    }

    /// Returns true if a node is indeed deleted
    ///
    /// Nodes do not delete themselves. Only parent nodes will delete their direct child node (if
    /// its a match).
    pub fn delete_one_by_value(&mut self, value: T) -> bool {
        if self.next_node.is_some() {
            let next_node_value = self
                .next_node
                .as_ref()
                .expect("Could not unwrap next_node for copying value")
                .borrow()
                .value;
            if next_node_value == value {
                let the_node_after = &self.next_node.as_ref().unwrap().borrow().next_node.clone();
                match the_node_after {
                    Some(node) => self.next_node = Some(Rc::clone(node)),
                    None => self.next_node = None,
                }
                true
            } else {
                self.next_node
                    .as_ref()
                    .expect("Could not unwrap next_node as mut to delete value")
                    .borrow_mut()
                    .delete_one_by_value(value)
            }
        } else {
            false
        }
    }

    pub fn len(&self) -> isize {
        let mut count = 1;
        if let Some(next_node) = &self.next_node {
            count += next_node.borrow().len();
        }
        count
    }

    /// Is the Node empty?
    ///
    /// In this design, nope, it never is.
    pub fn is_empty(&self) -> bool {
        false
    }
}

impl<T> PartialEq for Node<T>
where
    T: Copy + Eq,
{
    /// Verify if 2 nodes are the same by comparing their values, and if all their consequent nodes
    /// match up equally.
    fn eq(&self, other: &Self) -> bool {
        if self.value != other.value {
            false
        } else if self.next_node.is_none() && other.next_node.is_none() {
            true
        } else if (self.next_node.is_none() && other.next_node.is_some())
            || (self.next_node.is_some() && other.next_node.is_none())
        {
            false
        } else {
            self.next_node
                .as_ref()
                .unwrap()
                .eq(other.next_node.as_ref().unwrap())
        }
    }
}

impl<T> Eq for Node<T> where T: Copy + Eq {}
