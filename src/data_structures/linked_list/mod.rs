mod node;

pub use node::Node;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
pub struct LinkedList<T: Copy + Eq> {
    head: Option<Rc<RefCell<Node<T>>>>,
    tail: Option<Rc<RefCell<Node<T>>>>,
}

impl<T> LinkedList<T>
where
    T: Copy + Eq,
{
    pub fn new(starting_value: T) -> Self {
        let rced_new_node = Rc::new(RefCell::new(Node::new(starting_value)));
        LinkedList {
            head: Some(Rc::clone(&rced_new_node)),
            tail: Some(Rc::clone(&rced_new_node)),
        }
    }

    pub fn from(values: &[T]) -> Self {
        let mut linked_list = LinkedList::new(values[0]);
        for (i, value) in values.iter().enumerate() {
            if i == 0 {
                continue;
            }

            linked_list.append(*value);
        }

        linked_list
    }

    pub fn get_head(&self) -> Option<Rc<RefCell<Node<T>>>> {
        if self.head.is_none() {
            None
        } else {
            Some(Rc::clone(self.head.as_ref().unwrap()))
        }
    }

    pub fn get_tail(&self) -> Option<Rc<RefCell<Node<T>>>> {
        if self.tail.is_none() {
            None
        } else {
            Some(Rc::clone(self.tail.as_ref().unwrap()))
        }
    }

    pub fn append(&mut self, value: T) {
        let new_tail_node_ref = Rc::new(RefCell::new(Node::new(value)));
        if self.tail.is_some() {
            self.tail
                .as_ref()
                .expect("Could not unwrap tail for appending")
                .borrow_mut()
                .append_node(Rc::clone(&new_tail_node_ref));
        }
        self.tail = Some(Rc::clone(&new_tail_node_ref));
    }

    pub fn prepend(&mut self, value: T) {
        let mut new_head_node = Node::new(value);
        if self.head.is_some() {
            new_head_node.next_node = Some(Rc::clone(
                self.head
                    .as_ref()
                    .expect("Could not unwrap head for prepend"),
            ));
        }
        self.head = Some(Rc::new(RefCell::new(new_head_node)));
    }

    pub fn contains(&self, value: T) -> bool {
        if let Some(head) = &self.head {
            if head.borrow().value == value {
                true
            } else {
                head.borrow().contains(value)
            }
        } else {
            false
        }
    }

    pub fn delete_one_by_value(&mut self, value: T) -> bool {
        if self.head.is_some() {
            if self
                .head
                .as_ref()
                .expect("Could not unwrap head for checking value")
                .borrow()
                .value
                == value
            {
                self.head = None;
                true
            } else {
                self.head
                    .as_ref()
                    .expect("Could not unwrap head for deleting nodes")
                    .borrow_mut()
                    .delete_one_by_value(value)
            }
        } else {
            false
        }
    }

    pub fn len(&self) -> isize {
        if self.head.is_some() {
            self.head.as_ref().unwrap().borrow().len()
        } else {
            0
        }
    }

    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }

    pub fn get_by_index(&self, index: isize) -> Option<Rc<RefCell<Node<T>>>> {
        // let's say we have 3 elements
        // 0 1 2
        // a b c
        // We want -1 to give c, -2 b, -3 a, -4 c
        // 3 to give a, 4 b, 5, c
        let self_len = self.len();
        if self_len == 0 {
            None
        } else {
            let mut wrapped_index = {
                let base = index % self_len;
                if base >= 0 {
                    base
                } else {
                    self_len + base
                }
            };

            let mut node = Rc::clone(self.head.as_ref().unwrap());
            while wrapped_index > 0 {
                // We're guaranteed to have a next node, always
                let borrow = Rc::clone(node.as_ref().borrow().next_node.as_ref().unwrap());
                node = Rc::clone(&borrow);
                wrapped_index -= 1;
            }

            Some(Rc::clone(&node))
        }
    }

    pub fn delete_one_by_index(&mut self, index: isize) -> bool {
        let self_len = self.len();
        if self_len == 0 {
            return false;
        }

        let wrapped_index = {
            let base = index % self_len;
            if base >= 0 {
                base
            } else {
                self_len + base
            }
        };

        if self_len == 1 {
            self.head = None;
            self.tail = None;
            true
        } else if wrapped_index == 0 {
            let second_node = self.get_by_index(1).unwrap();
            self.head = Some(Rc::clone(&second_node));
            true
        } else {
            let target_node = self.get_by_index(wrapped_index).unwrap();
            let prev_node = self.get_by_index(wrapped_index - 1).unwrap();
            if target_node.borrow().next_node.is_some() {
                prev_node.borrow_mut().next_node =
                    Some(Rc::clone(target_node.borrow().next_node.as_ref().unwrap()));
            } else {
                prev_node.borrow_mut().next_node = None;
            }

            if wrapped_index == self_len - 1 {
                self.tail = Some(Rc::clone(&prev_node));
            }

            true
        }
    }

    pub fn pop(&mut self) -> bool {
        self.delete_one_by_index(self.len() - 1)
    }
}

#[cfg(test)]
mod test {
    use super::LinkedList;

    #[test]
    fn linked_list_contains() {
        let linked_list = LinkedList::from(&vec![4, 3, 7, 1]);
        assert!(linked_list.contains(4));
        assert!(linked_list.contains(1));
        assert!(!linked_list.contains(-1));
    }

    #[test]
    fn linked_list_append() {
        let mut linked_list = LinkedList::from(&vec![0, 1, 2, 3]);
        assert_eq!(
            linked_list
                .head
                .as_ref()
                .expect("Could not unwrap head for test check")
                .borrow()
                .value,
            0
        );
        assert_eq!(
            linked_list
                .tail
                .as_ref()
                .expect("Could not unwrap tail for test check")
                .borrow()
                .value,
            3
        );
        linked_list.append(4);
        assert_eq!(
            linked_list
                .head
                .expect("Could not unwrap head for test check")
                .borrow()
                .value,
            0
        );
        assert_eq!(
            linked_list
                .tail
                .as_ref()
                .expect("Could not unwrap tail for test check")
                .borrow()
                .value,
            4
        );
    }

    #[test]
    fn linked_list_prepend() {
        let mut linked_list = LinkedList::from(&vec![0, 1, 2, 3]);
        assert_eq!(
            linked_list
                .head
                .as_ref()
                .expect("Could not unwrap head for test check")
                .borrow()
                .value,
            0
        );
        assert_eq!(
            linked_list
                .tail
                .as_ref()
                .expect("Could not unwrap tail for test check")
                .borrow()
                .value,
            3
        );
        linked_list.prepend(-1);
        assert_eq!(
            linked_list
                .head
                .as_ref()
                .expect("Could not unwrap head for test check")
                .borrow()
                .value,
            -1
        );
        assert_eq!(
            linked_list
                .tail
                .as_ref()
                .expect("Could not unwrap tail for test check")
                .borrow()
                .value,
            3
        );
    }

    #[test]
    fn count_nodes() {
        assert_eq!(LinkedList::from(&vec![0, 1, 2, 3]).len(), 4);
        assert_eq!(LinkedList::from(&vec![0, 1, 2]).len(), 3);
    }

    #[test]
    fn get_by_index() {
        let linked_list = LinkedList::from(&vec![0, 1, 2, 3]);
        assert_eq!(linked_list.get_by_index(0).unwrap().borrow().value, 0);
        assert_eq!(linked_list.get_by_index(2).unwrap().borrow().value, 2);
        assert_eq!(linked_list.get_by_index(-1).unwrap().borrow().value, 3);
        assert_eq!(linked_list.get_by_index(5).unwrap().borrow().value, 1);
    }

    #[test]
    fn delete_one_by_index() {
        let mut linked_list = LinkedList::from(&vec![0, 1, 2, 3]);
        assert!(linked_list.contains(1));
        assert!(linked_list.delete_one_by_index(1));
        assert!(!linked_list.contains(1));
        assert!(linked_list.delete_one_by_index(1));
        assert!(linked_list.delete_one_by_index(1));
        assert!(linked_list.delete_one_by_index(1));
        assert!(!linked_list.delete_one_by_index(1));
        assert_eq!(linked_list.len(), 0);
    }

    #[test]
    fn pop() {
        let mut linked_list = LinkedList::from(&vec![0, 1, 2, 3]);
        assert!(linked_list.contains(3));
        assert_eq!(linked_list.tail.as_ref().unwrap().borrow().value, 3);
        assert!(linked_list.pop());
        assert!(!linked_list.contains(3));
        assert_eq!(linked_list.tail.as_ref().unwrap().borrow().value, 2);
        assert!(linked_list.pop());
        assert!(linked_list.pop());
        assert!(linked_list.pop());
        assert!(!linked_list.pop());
        assert_eq!(linked_list.len(), 0);
    }
}
