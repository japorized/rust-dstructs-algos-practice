extern crate lib;

use lib::math::factorial;

fn main() {
    println!("{}", factorial::classic_recursion(100));
}
