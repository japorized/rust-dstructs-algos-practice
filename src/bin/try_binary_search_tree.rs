extern crate lib;

use lib::data_structures::binary_search_tree::BinarySearchTree;

fn main() {
    let mut bst = BinarySearchTree::new();
    bst.insert(5);
    bst.insert(1);
    bst.insert(4);
    bst.insert(3);
    bst.insert(6);

    println!("{}", bst);
}
