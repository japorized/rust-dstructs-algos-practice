extern crate lib;

use lib::math::sieve_of_eratosthenes::get_prime_numbers_up_to_n;

fn main() {
    println!("{:?}", get_prime_numbers_up_to_n(120));
}
