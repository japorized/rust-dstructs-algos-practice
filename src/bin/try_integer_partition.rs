extern crate lib;

use lib::math::integer_partition::{get_integer_partition, Partition};

fn main() {
    let args = std::env::args().collect::<Vec<String>>();
    let val = if let Some(v) = args.get(1) {
        v.parse::<usize>()
            .expect("Could not convert argument to usize")
    } else {
        10
    };
    let int_partition = get_integer_partition(val);
    let mut sorted_int_partition = int_partition.into_iter().collect::<Vec<Partition>>();
    sorted_int_partition.sort();
    sorted_int_partition.reverse();
    for p in &sorted_int_partition {
        println!(
            "{}",
            p.values
                .iter()
                .map(|i| format!("{}", i))
                .collect::<Vec<String>>()
                .join(" + ")
        );
    }
}
