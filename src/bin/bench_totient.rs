use lib::math::eulers_totient_function;
use std::time::Instant;

fn main() {
    let time1 = Instant::now();
    eulers_totient_function::totient_phi(100900);
    println!("Time elapsed for totient_phi: {:.2?}", time1.elapsed());

    let time2 = Instant::now();
    eulers_totient_function::totient_phi_alt(100900);
    println!("Time elapsed for totient_phi_alt: {:.2?}", time2.elapsed());
}
